

from   UtilityUnits import *
import CarpetASCIII as CARPET
import CarpetH5     as H5
import UtilityPostProcessing   as PP
import UtilityAnalysis         as Analysis
import UtilityImages           as IM
import CarpetSimulationData    as CC
import UtilityEOS              as EOS
############################################
##  Not needed
import UtilityPostProcessing as PP
import UtilityEOS            as EOS

print("# --------------------------------------------------------------------------------------")
print("# Consider setting: IM.SetBaseDir('/numrel/storageQ02/Work_BNS16/PostProcessing/kepler')")
print("#                   IM.SetBaseImageDir('./Images')")
print("# --------------------------------------------------------------------------------------")

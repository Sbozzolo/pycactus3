#!/usr/bin/env python 

# constants, in SI
G     = 6.673e-11       # m^3/(kg s^2)
c     = 299792458.0       # m/s
M_sun = 1.98892e30      # kg
mu0   = 1.2566370614e-6 # Newton/Ampere^2
Kb    = 1.3806488e-23   # Joule/K
Mparsec = 3.08567758e19 # km 
cCGS    =  299792458.0 * 100 # cm/s 
eV      = 1.602176565e-19    # Joule
MeV     = 1.602176565e-13    # Joule
MeVCGS  = 1.602176565e-6     # g cm^2/s^2
fermi   = 1e-15              # metri  
fermiCGS= 1e-13              # metri  
##----------------------------------
## The Barion Mass used by RNS
##----------------------------------
Mbaryon    = 1.66e-27   # kg 
MbaryonCGS = 1.66e-24   # g
#-------------------------------------------
# conversion factors
#-------------------------------------------
CU_to_km   = M_sun*G/(1000*c*c)                  # km
CU_to_ms   = (1000*M_sun*G/(c*c*c))              # ms
CU_to_s    = (M_sun*G/(c*c*c))                   # s
CU_to_dens = c*c*c*c*c*c / (G*G*G * M_sun*M_sun) # kg/m^3
CU_to_press= c**6 / (G**3 * M_sun**2) *c**2      # kg m/s^2 /m^2
CU_to_Tesla = c**4 / M_sun / G**(1.5)* mu0**(0.5)
CU_to_Gauss = c**4 / M_sun / G**(1.5)* mu0**(0.5) * 10000
CU_to_energy    = M_sun*c*c                # kg m^2/s^2
CU_to_densCGS   = CU_to_dens  *1000/100**3 # g/cm^3
CU_to_pressCGS  = CU_to_press *1000/100    # g /s^2 / cm
# ------------------------------------------------------------
# All expeession in CU refer to unit mass (a-dimensional) 
# where G=c=M_sun=1
# ------------------------------------------------------------

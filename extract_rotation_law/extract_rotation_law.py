#!/usr/bin/env python3

# Extract the rotation law from a simulation

# Author: Gabriele Bozzola (sbozzolo)
# Email: sbozzolator@gmail.com
# Version: 1.0
# First Stable: 27/08/17
# Last Edit: 29/08/17

import argparse
import sys
import os
import shutil
import time
import datetime
import re
import glob
from timeit import default_timer as timer
from datetime import timedelta as timedelta
import subprocess
from scipy.optimize import curve_fit
from scipy.optimize import minimize

import numpy as np
import matplotlib as mlp
mlp.use('Agg')
import matplotlib.pyplot as plt
from matplotlib import rcParams
import matplotlib.ticker as mtick
rcParams['xtick.direction'] = 'in'
rcParams['ytick.direction'] = 'in'


# Include PyCactus3
sys.path.append('/home/kepler/bozzola/PyCactus3')
import CarpetASCIII as CARPET
import CarpetH5     as H5
import UtilityPostProcessing as PP


from matplotlib2tikz import save as tikz_save


# Model!

# These are the functions used to interpolate the coefficients

def omega_c_try(t, a, b, c, d):
    return a/(1 + b*t + d*t*t) + c

def b_try(t, a, b, c, d):
    return a/(1 + b*t + d*t**2) + c

def Bsq_try(t, a, b, c, d):
    return a/(1 + b*t + d*t**2) + c

def omega(t, F):
    return -Bsq_try(t, *Bsq_fit[0])*F*F + b_try(t, *b_fit[0])*F + omega_c_try(t, *omega_c_fit[0])

################################################

# if plt.rcParams["text.usetex"] is False:
#     plt.rcParams["text.usetex"] = True

# if plt.rcParams["text.latex.unicode"] is False:
#     plt.rcParams["text.latex.unicode"] = True


# Function to compute phi average of function F
def phi_average(F, N, x_grid, y_grid):

    theta      = np.linspace(-np.pi/2, np.pi/2, N)

    averaged_f = np.zeros(len(x_grid))

    f = F[:,(len(F[0][:])-len(x_grid)):]

    for j,X in enumerate(x_grid):

        approximated_f = np.zeros(N)

        for i,th in enumerate(theta):

            x_target = X*np.cos(th)
            y_target = X*np.sin(th)

            i0 = np.argmax(x_grid > x_target)
            i1 = i0 - 1
            ix_box = np.array([i1, i0])
            x_box = np.array([x_grid[i1], x_grid[i0]])

            j0 = np.argmax(y_grid > y_target)
            j1 = j0 - 1

            iy_box = np.array([j1, j0])
            y_box = np.array([y_grid[j1], y_grid[j0]])

            weights = np.array([[1/((x - x_target)*(x - x_target) + (y - y_target)*(y - y_target) + 1e-20) for y in x_box] for x in y_box])
            comp_f = np.array([[f[x,y] for y in ix_box] for x in iy_box])
            normalisation = 1/np.sum(weights)
            weights *= normalisation

            approximated_f[i] = np.sum(np.multiply(weights, comp_f))

        averaged_f[j] = np.mean(approximated_f)

    return averaged_f


def plot_and_save(variable, name, data_folder, pic_folder):
    """Plot variable against time and save data in folder"""
    if v: print("Plotting", name, end = " ... ")

    fig, ax = plt.subplots()
    ax.plot(time, variable)
    ax.set_xlabel("Time [ms]")
    ax.set_ylabel(name)
    fig.tight_layout()

    # Save pdf
    fig.savefig(pic_folder + "/" + name + ".pdf", format = 'pdf', dpi = 1200)

    # Save tikz with dirty trick to not print useless information
    sys.stdout = open(os.devnull, "w")
    tikz_save(pic_folder + "/" + name + ".tikz", figureheight = '\\figureheight',
                  figurewidth = '\\figurewidth', figure = fig)
    sys.stdout = sys.__stdout__

    plt.close(fig)
    if v: print("Done!")
    if v: print("Created plots:", pic_folder + "/" + name + ".pdf and",
                pic_folder + "/" + name + ".tikz")

    np.savetxt(data_folder + "/" + name + ".dat", variable)
    if v: print("Created data file:", data_folder + "/" + name + ".dat")

def extract_and_analyze_data(descrpitor, iteration, iteration_length, sim_name):
    """Extract data from the h5 file corresponding to iteration,
    then analyze data and plot them"""

    start_time = timer()
    dataXY = H5.CreateJoinedHDF5data(descriptor, iteration, max_rl, dataXY_file)
    dataX  = H5.Extract1dSlice(dataXY, size = 2)
    dataY  = H5.Extract1dSlice(dataXY, size = 2, dir = 1)

    PP.PostProcessing3dFull(dataX, kind = "X")
    PP.PostProcessing3dFull(dataY, kind = "Y")
    PP.PostProcessing3dFull(dataXY, kind = "XY")

    # Index of the center
    if v: print("Finding center of the grid", end = " ... ")
    zerox = np.where(dataX['X[0]'] == 0)[0][-1]
    zeroy = np.where(dataY['X[1]'] == 0)[0][-1]
    if v: print("Done!")

    # Surface is the index of the last element with rho > than 0.01 rho_central
    if v: print("Finding surface with density", surface_density, "rho_c", end = " ... ")
    surfacex = np.where(dataX['rho'] >= dataX['rho'][zerox]*surface_density)[0][-1]
    surfacey = np.where(dataY['rho'] >= dataY['rho'][zeroy]*surface_density)[0][-1]
    if v: print("Done!")

    if args.phi_average:
        if v: print("Extracting phi average ", end = '...')
        x_grid = dataX['X[0]'][zerox:]
        y_grid = dataY['X[1]']

        F_phi_averaged       = phi_average(dataXY['F'], 180, x_grid, y_grid)
        rho_phi_averaged     = phi_average(dataXY['rho'], 180, x_grid, y_grid)
        omega_phi_averaged   = phi_average(dataXY['Omega'], 180, x_grid, y_grid)
        betaphi_phi_averaged = phi_average(dataXY['betaphi'], 180, x_grid, y_grid)
        surface              = np.where(rho_phi_averaged >= dataX['rho'][zerox]*surface_density)[0][-1]

        np.savetxt(os.path.join(rl_data_folder, str(iteration).zfill(iteration_length) + "_" +
                                str(dataY['time']) + "_" + str(surface) + "_rho_phi.txt.gz"), rho_phi_averaged)
        np.savetxt(os.path.join(rl_data_folder, str(iteration).zfill(iteration_length) + "_" +
                                str(dataY['time']) + "_" + str(surface) + "_F_phi.txt.gz"), F_phi_averaged)
        np.savetxt(os.path.join(rl_data_folder, str(iteration).zfill(iteration_length) + "_" +
                                str(dataY['time']) + "_" + str(surface) + "_Omega_phi.txt.gz"), omega_phi_averaged)
        np.savetxt(os.path.join(rl_data_folder, str(iteration).zfill(iteration_length) + "_" +
                                str(dataY['time']) + "_" + str(surface) + "_betaphi_phi.txt.gz"), betaphi_phi_averaged)

        fig, (ax1, ax2) = plt.subplots(2)
        ax1.plot(omega_phi_averaged/CU_to_ms, F_phi_averaged, c = 'orange')
        ax1.text(2, 2, str(round(dataX['time']*CU_to_ms,3)) + " ms")
        ax1.set_ylabel("F")
        ax1.set_xlabel("Omega [kHz]")
        ax1.axvline(x = omega_phi_averaged[surface]/CU_to_ms, color = 'orange')
        ax1.set_xlim([0,10])
        ax1.set_ylim([0,10])

        ax2.plot(x_grid*CU_to_km, rho_phi_averaged*CU_to_dens_CGS, c = 'orange')
        ax2.set_ylabel("rho [g/cm^3]")
        ax2.set_xlabel("R [km]")
        ax2.axvline(x = x_grid[surface]*CU_to_km, color = 'orange')
        ax2.set_ylim([0, 1.5e15])
        ax2.set_xlim([0, np.amax(x_grid)])

        fig.savefig(os.path.join(rl_phi_pics_folder, str(iteration).zfill(iteration_length)) + ".png")
        fig.tight_layout()
        plt.close(fig)
        if v: print("Done!")
        if v: print("Created data files rho_phi and F_phi for iteration:", iteration)
        if v: print("Created plot:", os.path.join(rl_phi_pics_folder, str(iteration).zfill(iteration_length)) + ".png")

    if v: print("Plotting and saving data", end = " ... ")
    # Global figure with the situation and two plots
    fig = plt.figure()
    ax1  = plt.subplot2grid((3,4), (0,0))
    ax2  = plt.subplot2grid((3,4), (1,1))
    ax3  = plt.subplot2grid((3,4), (1,0))
    ax4  = plt.subplot2grid((3,4), (0,1))
    ax5  = plt.subplot2grid((3,4), (1,2))
    ax6  = plt.subplot2grid((3,4), (0,2))
    ax7  = plt.subplot2grid((3,4), (2,0))
    ax8  = plt.subplot2grid((3,4), (2,1))
    ax9  = plt.subplot2grid((3,4), (2,2))
    ax10 = plt.subplot2grid((3,4), (0,3))
    ax11 = plt.subplot2grid((3,4), (1,3))
    ax12 = plt.subplot2grid((3,4), (2,3))

    # Densities
    if (args.plot_sym): dataXY = H5.ExtendData2dPYSIM(dataXY)
    ax1.axis('equal')
    ax1.set_xlim([np.amin(dataXY['X[0]']), np.amax(dataXY['X[0]'])])
    ax1.set_ylim([np.amin(dataXY['X[1]']), np.amax(dataXY['X[1]'])])
    ax1.contour(dataXY['X[0]']*CU_to_km,dataXY['X[1]']*CU_to_km,dataXY['rho']*CU_to_dens_CGS)
    ax1.pcolor(dataXY['X[0]']*CU_to_km,dataXY['X[1]']*CU_to_km,dataXY['rho']*CU_to_dens_CGS,
               cmap = 'PuRd', norm = mlp.colors.Normalize())
    ax1.set_xlabel("x [km]")
    ax1.set_ylabel("y [km]")
    ax1.text(-10, 35, str(round(dataX['time']*CU_to_ms,3)) + " ms", color = 'black')
    ax1.set_title(sim_name)

    ax2.set_xlabel("R [km]")
    ax2.set_ylabel("Omega [kHz]")
    ax2.set_xlim([0, np.amax(dataX['X[0]'])*CU_to_km])
    ax2.set_ylim([0,12])
    ax2.axvline(x = dataX['X[0]'][surfacex]*CU_to_km, color = 'blue')
    ax2.axvline(x = dataY['X[1]'][surfacey]*CU_to_km, color = 'red')
    ax2.plot(dataX['X[0]'][zerox:]*CU_to_km, dataX['Omega'][zerox:]/CU_to_ms, color = 'blue')
    ax2.plot(dataY['X[1]'][zeroy:]*CU_to_km, dataY['Omega'][zeroy:]/CU_to_ms, color = 'red')
    if (args.phi_average): ax2.plot(x_grid*CU_to_km, omega_phi_averaged/CU_to_ms, color = 'orange')
    if (args.phi_average): ax2.plot(x_grid*CU_to_km, -betaphi_phi_averaged/CU_to_ms, color = 'orange')
    if (args.phi_average): ax2.axvline(x = x_grid[surface]*CU_to_km, color = 'orange')
    ax2.plot(dataX['X[0]'][zerox:]*CU_to_km, -dataX['betaphi'][zerox:]/CU_to_ms, color = 'blue', ls = 'dashed')
    ax2.plot(dataY['X[1]'][zeroy:]*CU_to_km, -dataY['betaphi'][zeroy:]/CU_to_ms, color = 'red', ls = 'dashed')

    ax3.set_xlabel("Omega [kHz]")
    ax3.set_ylabel("F(Omega) [ms]")
    ax3.set_ylim([0,0.05])
    # ax3.set_xlim([np.amin(d['Omega'][zero:])/CU_to_ms, np.amax(d['Omega'][zero:])/CU_to_ms])
    ax3.set_xlim([0,13])
    ax3.plot(dataX['Omega'][zerox:]/CU_to_ms, dataX['F'][zerox:]*CU_to_ms, color = 'blue')
    ax3.plot(dataY['Omega'][zeroy:]/CU_to_ms, dataY['F'][zeroy:]*CU_to_ms, color = 'red')
    ax3.axvline(x = dataX['Omega'][surfacex]/CU_to_ms, color = 'blue')
    ax3.axvline(x = dataY['Omega'][surfacey]/CU_to_ms, color = 'red')
    if (args.phi_average): ax3.plot(omega_phi_averaged/CU_to_ms, F_phi_averaged*CU_to_ms, color = 'orange')
    if (args.phi_average): ax3.axvline(x = omega_phi_averaged[surface]/CU_to_ms, color = 'orange')
    # ax3.axvspan(d['Omega'][surface]/CU_to_ms, d['Omega'][len(d['Omega']) - 1]/CU_to_ms, alpha = 0.1, color = 'blue')

    ax4.set_xlabel("R [km]")
    ax4.set_ylabel("rho [g/cm^3]")
    ax4.set_xlim([0, np.amax(dataX['X[0]'])*CU_to_km])
    ax4.set_ylim([0, 1.5e15])
    ax4.axvline(x = dataX['X[0]'][surfacex]*CU_to_km, color = 'blue')
    ax4.axvline(x = dataY['X[1]'][surfacey]*CU_to_km, color = 'red')
    ax4.plot(dataX['X[0]'][zerox:]*CU_to_km, dataX['rho'][zerox:]*CU_to_dens_CGS, color = 'blue')
    ax4.plot(dataY['X[1]'][zeroy:]*CU_to_km, dataY['rho'][zeroy:]*CU_to_dens_CGS, color = 'red')
    if (args.phi_average): ax4.plot(x_grid*CU_to_km, rho_phi_averaged*CU_to_dens_CGS, c = 'orange')
    if (args.phi_average): ax4.axvline(x = x_grid[surface]*CU_to_km, color = 'orange')

    ax5.set_xlabel("R [km]")
    ax5.set_ylabel("F(Omega) [ms]")
    ax5.set_xlim([0, np.amax(dataX['X[0]'])*CU_to_km])
    ax5.set_ylim([0,0.05])
    ax5.axvline(x = dataX['X[0]'][surfacex]*CU_to_km, color = 'blue')
    ax5.axvline(x = dataY['X[1]'][surfacey]*CU_to_km, color = 'red')
    ax5.plot(dataX['X[0]'][zerox:]*CU_to_km, dataX['F'][zerox:]*CU_to_ms, color = 'blue')
    ax5.plot(dataY['X[1]'][zeroy:]*CU_to_km, dataY['F'][zeroy:]*CU_to_ms, color = 'red')
    if (args.phi_average): ax5.plot(x_grid*CU_to_km, F_phi_averaged*CU_to_ms, c = 'orange')
    if (args.phi_average): ax5.axvline(x = x_grid[surface]*CU_to_km, color = 'orange')

    ax6.set_xlabel("R [km]")
    ax6.set_ylabel("rho [g/cm^3]")
    ax6.set_xlim([np.amin(dataX['X[0]'])*CU_to_km, 4])
    ax6.set_ylim([0.1e15,1.5e15])
    # ax6.axvline(x = dataX['X[0]'][surfacex]*CU_to_km, color = 'blue')
    # ax6.axvline(x = dataY['X[1]'][surfacey]*CU_to_km, color = 'red')
    # ax6.axvline(x = -dataX['X[0]'][surfacex]*CU_to_km, color = 'blue')
    # ax6.axvline(x = -dataY['X[1]'][surfacey]*CU_to_km, color = 'red')
    ax6.axvline(x = 0, color = 'black', ls = 'dashed')
    ax6.plot(dataX['X[0]']*CU_to_km, dataX['rho']*CU_to_dens_CGS, color = 'blue')
    ax6.plot(dataY['X[1]']*CU_to_km, dataY['rho']*CU_to_dens_CGS, color = 'red')

    ax7.set_xlabel("R [km]")
    ax7.set_ylabel("j [CU]")
    ax7.set_xlim([np.amin(dataX['X[0]'])*CU_to_km, np.amax(dataX['X[0]'])*CU_to_km])
    ax7.set_ylim([0,10])
    ax7.axvline(x = dataX['X[0]'][surfacex]*CU_to_km, color = 'blue')
    ax7.axvline(x = dataY['X[1]'][surfacey]*CU_to_km, color = 'red')
    ax7.axvline(x = -dataX['X[0]'][surfacex]*CU_to_km, color = 'blue')
    ax7.axvline(x = -dataY['X[1]'][surfacey]*CU_to_km, color = 'red')
    ax7.plot(dataX['X[0]']*CU_to_km, dataX['uphi_con']*(dataX['e0'])/dataX['rho'], color = 'blue')
    ax7.plot(dataY['X[1]']*CU_to_km, dataY['uphi_con']*(dataY['e0'])/dataY['rho'], color = 'red')

    ax8.set_xlabel("R [km]")
    ax8.set_ylabel("u_phi [CU]")
    ax8.set_xlim([np.amin(dataX['X[0]'])*CU_to_km, np.amax(dataX['X[0]'])*CU_to_km])
    ax8.set_ylim([0,10])
    ax8.axvline(x = dataX['X[0]'][surfacex]*CU_to_km, color = 'blue')
    ax8.axvline(x = dataY['X[1]'][surfacey]*CU_to_km, color = 'red')
    ax8.axvline(x = -dataX['X[0]'][surfacex]*CU_to_km, color = 'blue')
    ax8.axvline(x = -dataY['X[1]'][surfacey]*CU_to_km, color = 'red')
    ax8.plot(dataX['X[0]']*CU_to_km, dataX['uphi_con'], color = 'blue')
    ax8.plot(dataY['X[1]']*CU_to_km, dataY['uphi_con'], color = 'red')

    ax9.set_xlabel("R [km]")
    ax9.set_ylabel("h [CU]")
    ax9.set_xlim([np.amin(dataX['X[0]'])*CU_to_km, np.amax(dataX['X[0]'])*CU_to_km])
    ax9.set_ylim([1,1.5])
    ax9.axvline(x = dataX['X[0]'][surfacex]*CU_to_km, color = 'blue')
    ax9.axvline(x = dataY['X[1]'][surfacey]*CU_to_km, color = 'red')
    ax9.axvline(x = -dataX['X[0]'][surfacex]*CU_to_km, color = 'blue')
    ax9.axvline(x = -dataY['X[1]'][surfacey]*CU_to_km, color = 'red')
    ax9.plot(dataX['X[0]']*CU_to_km, dataX['e0']/dataX['rho'], color = 'blue')
    ax9.plot(dataY['X[1]']*CU_to_km, dataY['e0']/dataY['rho'], color = 'red')

    ax10.set_xlabel("R [km]")
    ax10.set_ylabel("u^phi [CU]")
    ax10.set_xlim([np.amin(dataX['X[0]'])*CU_to_km, np.amax(dataX['X[0]'])*CU_to_km])
    ax10.set_ylim([0,0.15])
    ax10.axvline(x = dataX['X[0]'][surfacex]*CU_to_km, color = 'blue')
    ax10.axvline(x = dataY['X[1]'][surfacey]*CU_to_km, color = 'red')
    ax10.axvline(x = -dataX['X[0]'][surfacex]*CU_to_km, color = 'blue')
    ax10.axvline(x = -dataY['X[1]'][surfacey]*CU_to_km, color = 'red')
    ax10.plot(dataX['X[0]']*CU_to_km, dataX['uphi'], color = 'blue')
    ax10.plot(dataY['X[1]']*CU_to_km, dataY['uphi'], color = 'red')

    ax11.set_xlabel("R [km]")
    ax11.set_ylabel("u^0 [CU]")
    ax11.set_xlim([np.amin(dataX['X[0]'])*CU_to_km, np.amax(dataX['X[0]'])*CU_to_km])
    ax11.set_ylim([1,2.5])
    ax11.axvline(x = dataX['X[0]'][surfacex]*CU_to_km, color = 'blue')
    ax11.axvline(x = dataY['X[1]'][surfacey]*CU_to_km, color = 'red')
    ax11.axvline(x = -dataX['X[0]'][surfacex]*CU_to_km, color = 'blue')
    ax11.axvline(x = -dataY['X[1]'][surfacey]*CU_to_km, color = 'red')
    ax11.plot(dataX['X[0]']*CU_to_km, dataX['u0'], color = 'blue')
    ax11.plot(dataY['X[1]']*CU_to_km, dataY['u0'], color = 'red')

    ax12.set_xlabel("R [km]")
    ax12.set_ylabel("e [CU]")
    ax12.set_xlim([np.amin(dataX['X[0]'])*CU_to_km, np.amax(dataX['X[0]'])*CU_to_km])
    ax12.set_ylim([0,0.0025])
    ax12.axvline(x = dataX['X[0]'][surfacex]*CU_to_km, color = 'blue')
    ax12.axvline(x = dataY['X[1]'][surfacey]*CU_to_km, color = 'red')
    ax12.axvline(x = -dataX['X[0]'][surfacex]*CU_to_km, color = 'blue')
    ax12.axvline(x = -dataY['X[1]'][surfacey]*CU_to_km, color = 'red')
    ax12.plot(dataX['X[0]']*CU_to_km, dataX['e0'], color = 'blue')
    ax12.plot(dataY['X[1]']*CU_to_km, dataY['e0'], color = 'red')

    fig.tight_layout()

    fig.savefig(os.path.join(rl_pics_folder, str(iteration).zfill(iteration_length)) + ".png")

    np.savetxt(os.path.join(rl_data_folder, str(iteration).zfill(iteration_length) + "_" +
                            str(dataX['time']) + "_" + str(surfacex) + "_Omega_x.txt.gz"), dataX['Omega'])
    np.savetxt(os.path.join(rl_data_folder, str(iteration).zfill(iteration_length) + "_" +
                            str(dataX['time']) + "_" + str(surfacex) + "_F_x.txt.gz"), dataX['F'])
    np.savetxt(os.path.join(rl_data_folder, str(iteration).zfill(iteration_length) + "_" +
                            str(dataY['time']) + "_" + str(surfacey) + "_Omega_y.txt.gz"), dataY['Omega'])
    np.savetxt(os.path.join(rl_data_folder, str(iteration).zfill(iteration_length) + "_" +
                            str(dataY['time']) + "_" + str(surfacey) + "_F_y.txt.gz"), dataY['F'])

    plt.close(fig)
    if v: print("Done!")

    if v: print("Created plot:", os.path.join(rl_pics_folder, str(iteration).zfill(iteration_length)) + ".png")

    if v: print("Created data files (Omega_x, F_x, Omega_y, F_y) for iteration:", iteration)

    end_time = timer()
    seconds = end_time - start_time

    if (args.parallel):
        i = np.where(iterations_h5 == iteration)[0][0] + 1
        m, s = divmod(seconds*(i_final - i)/num_cores, 60)
        h, m = divmod(m, 60)
        if ((i - 1) % num_cores == 0):
            print("Status:", format(100*iteration/last_iteration,'.3f'), "% completed!")
            print("Estimated remaining time: {:02d} hours, {:02d} minutes and {:02d} seconds".format(int(h),int(m),int(s)))

    return seconds

# Parse command line arguments

parser = argparse.ArgumentParser()

parser.add_argument("-i", "--input", type = str, required = True,
                    help = "set simulation folder")
parser.add_argument("-t", "--time", type = float, required = True,
                    help = "set final time")
parser.add_argument("-q", "--quadrupole", type = str,
                    help = "which quadrupole use?", choices = ["Qxx", "Qyy", "Qxy"])
parser.add_argument("-s", "--surface", type = float,
                    help = "surface density (in percent)")
parser.add_argument("-j", "--intervals", type = int,
                    help = "number of subintervals")
parser.add_argument("-c", "--cut", type = float,
                    help = "lower cut on F")
parser.add_argument("-p", "--periods", type = float,
                    help = "periods of the running average")
parser.add_argument("--no-data-extraction", action = "store_true",
                    help = "data are already extracted")
parser.add_argument("--phi-average", action = "store_true",
                    help = "compute phi average")
parser.add_argument("--error-bars", action = "store_true",
                    help = "plot error bars")
parser.add_argument("--plot-sym", action = "store_true",
                    help = "plot unrolling the pi-symmetry")
parser.add_argument("--parallel", action = "store_true",
                    help = "enable some parallelism (EXPERIMENTAL!)")
parser.add_argument("-v", "--verbose", action = "store_true")
parser.add_argument('--version', action='version', version='%(prog)s 1.0')

if len(sys.argv) == 1:
    parser.print_help()
    sys.exit(1)

args = parser.parse_args()

sim_folder        = args.input
sim_name          = sim_folder.split('/')[-1]
out_folder        = os.path.join(os.getcwd(), sim_name)
h5_folder         = os.path.join(out_folder, "h5")
q_data_folder     = os.path.join(out_folder, "q_data")
q_pics_folder     = os.path.join(out_folder, "q_pics")
rl_data_folder    = os.path.join(out_folder, "rl_data")
rl_av_data_folder = os.path.join(out_folder, "rl_av_data")

# This is done because ImageMagick has some problem with
# the full paths...
rl_pics_suffix     = "rl_pics"
rl_phi_pics_suffix = "rl_phi_pics"
rl_av_pics_suffix  = "rl_av_pics"
rl_pics_folder     = os.path.join(out_folder, rl_pics_suffix)
rl_phi_pics_folder = os.path.join(out_folder, rl_phi_pics_suffix)
rl_av_pics_folder  = os.path.join(out_folder, rl_av_pics_suffix)
model_folder       = os.path.join(rl_av_pics_folder, "model")
v                  = args.verbose
nd                 = args.no_data_extraction

if (nd and (not os.path.isdir(sim_folder))):
    print("Post processed data not existing!")
    sys.exit(1)

final_time     = args.time

# Default surface
if (args.surface == None):
    surface_density = 0.01
else:
    surface_density = args.surface

# Default periods
if (args.periods == None):
    num_periods = 2
else:
    num_periods = args.periods

# Default number of subintervals for averaging F
if (args.intervals == None):
    num_intervals = 100
else:
    num_intervals = args.intervals

# Default number of subintervals for averaging F
if (args.cut == None):
    cut_f = 1
else:
    cut_f = args.cut

# Constants, in SI
G     = 6.673e-11   # m^3/(kg s^2)
c     = 299792458   # m/s
M_sun = 1.98892e30  # kg

# Conversion factors
CU_to_km   = M_sun*G/(1000*c*c)                  # km
CU_to_ms   = (1000*M_sun*G/(c*c*c))              # ms
CU_to_dens = c*c*c*c*c*c / (G*G*G * M_sun*M_sun) # kg/m^3
CU_to_dens_CGS = CU_to_dens *1000/100**3         # g/cm^3


################# The program starts here #########################

# Check if simulation folder exists
if (not os.path.isdir(sim_folder)):
    print("Simulation folder", sim_folder, "not existing!")
    sys.exit(1)

print("Working on simulation:", sim_name)
print("Output will be saved in the folder:", out_folder)


if (not nd):
    # Check if the folder already exists, in that case, delete the content
    # expect the h5 files, which are important
    if (os.path.exists(out_folder)):
        print("\033[1mWarning:\033[0m the output folder", out_folder, "already"
               + "exists! Except the h5 files, all the other content will be deleted!")

        # Remove q_data folder
        shutil.rmtree(q_data_folder)
        if v: print("Removed", q_data_folder)

        # Remove q_data folder
        shutil.rmtree(q_pics_folder)
        if v: print("Removed", q_pics_folder)

        # Remove rl_data folder
        shutil.rmtree(rl_data_folder)
        if v: print("Removed", rl_data_folder)

        # Remove rl_pics folder
        shutil.rmtree(rl_pics_folder)
        if v: print("Removed", rl_pics_folder)

        # Remove rl_data folder
        shutil.rmtree(rl_av_data_folder)
        if v: print("Removed", rl_data_folder)

        # Remove rl_av_pics folder
        shutil.rmtree(rl_av_pics_folder)
        if v: print("Removed", rl_pics_folder)

        # Remove rl_phi_pics folder
        shutil.rmtree(rl_phi_pics_folder)
        if v: print("Removed", rl_phi_pics_folder)

    else:
        # Create output folder
        os.mkdir(out_folder)
        if v: print("Created", out_folder)

        # Create h5 folder
        os.mkdir(h5_folder)
        if v: print("Created", h5_folder)

    # Create data folders
    os.mkdir(q_data_folder)
    if v: print("Created", q_data_folder)
    os.mkdir(rl_data_folder)
    if v: print("Created", rl_data_folder)
    os.mkdir(q_pics_folder)
    if v: print("Created", q_pics_folder)
    os.mkdir(rl_pics_folder)
    if v: print("Created", rl_pics_folder)
    os.mkdir(rl_phi_pics_folder)
    if v: print("Created", rl_phi_pics_folder)
    os.mkdir(rl_av_pics_folder)
    if v: print("Created", rl_data_folder)
    os.mkdir(rl_av_data_folder)
    if v: print("Created", rl_av_pics_folder)
    os.mkdir(model_folder)
    if v: print("Created", model_folder)


if (nd):
    # Remove rl_av_data folder
    if (os.path.exists(rl_av_data_folder)): shutil.rmtree(rl_av_data_folder)
    if v: print("Removed", rl_av_data_folder)
    # Remove rl_av_pics folder
    if (os.path.exists(rl_av_pics_folder)): shutil.rmtree(rl_av_pics_folder)
    if v: print("Removed", rl_av_pics_folder)

    os.mkdir(rl_av_data_folder)
    if v: print("Created", rl_av_pics_folder)
    os.mkdir(rl_av_pics_folder)
    if v: print("Created", rl_av_pics_folder)
    os.mkdir(model_folder)
    if v: print("Created", model_folder)

# Reading the scalars
h5_scalar_file = os.path.join(h5_folder, sim_name + "_scalar.h5")
if v: print("Looking for scalar h5 file:", h5_scalar_file)

# Check if the file exists, if is there, read it
if (os.path.isfile(h5_scalar_file)):
    print("Scalar h5 file found! Reading it", end = " ... ")
    scalar_data = H5.ReadScalarHDF5(h5_scalar_file)
    print("Done!")
else:
    print("Scalar h5 file not found! Producing it")
    scalar_data = dict()
    if v: print("Producing scalar")
    scalar_data['Sc']      = CARPET.ReadASCIIFileRE(sim_folder)
    if v: print("Producing maximum")
    scalar_data['ScMAX']   = CARPET.ReadASCIIFileRE(sim_folder,'maximum.asc')
    if v: print("Producing minimum")
    scalar_data['ScMIN']   = CARPET.ReadASCIIFileRE(sim_folder,'minimum.asc')
    # if v: print("Producing norm 1")
    # scalar_data['ScNorm1'] = CARPET.ReadASCIIFileRE(sim_folder,'norm1.asc')
    # if v: print("Producing norm 2")
    # scalar_data['ScNorm2'] = CARPET.ReadASCIIFileRE(sim_folder,'norm2.asc')
    # Save the output
    H5.WriteScalarHDF5(h5_scalar_file, scalar_data)
    print("Scalar h5 file written in", h5_scalar_file)


h5_scalar_file_pp = os.path.join(h5_folder, sim_name + "_scalar_pp.h5")
if v: print("Looking for scalar post-processed h5 file:", h5_scalar_file_pp)

# Check if the file exists, if is there, read it
if (os.path.isfile(h5_scalar_file_pp)):
    print("Scalar post-processed h5 file found! Reading it", end = " ... ")
    scalar_data_pp = H5.ReadScalarHDF5(h5_scalar_file_pp)
    print("Done!")
else:
    print("Scalar post-processed h5 file not found! Producing it", end = " ... ")
    scalar_data_pp = PP.PostProcessingScalar(scalar_data)
    # Save the output
    H5.WriteScalarHDF5(h5_scalar_file_pp, scalar_data_pp)
    print("Done!")
    print("Scalar post-processed h5 file written in", h5_scalar_file_pp)


# Compute some variables and save data files
iterations     = [int(x) for x in scalar_data['ScMAX']['iteration']]
if (not nd):
    np.savetxt(q_data_folder + "/iterations.dat", iterations, 'd')
    if v: print("Created data file:", q_data_folder + "/iterations.dat")
time           = CU_to_ms * scalar_data['ScMAX']['time']
if (not nd):
    np.savetxt(q_data_folder + "/time.dat", time)
    if v: print("Created data file:", q_data_folder + "/time.dat")
tot_time       = time[-1]

# Print some stats
print("")
print("Simulation:", sim_name)
print("Number of iterations:", iterations[-1])
print("Total time: {:.3f} ms".format(tot_time))
print("")

# Check if final_time too large
if (final_time > tot_time):
    print("\033[1mWarning:\033[0m final too large! Setting to {:.3f} ms".format(tot_time))
    final_time = tot_time

# Find index corresponding to final_time
final_time_index = np.where(time > final_time)[0][0]

# Shrinking arrays
time       = time[:final_time_index]
iterations = iterations[:final_time_index]

if v: print("Final iteration:", iterations[-1])
if v: print("Final time: {:.3f} ms".format(time[-1]))

# Compute beta and save data file, keeping only the first part
if v: print("Computing T", end = " ... ")
T  = scalar_data['ScMAX']['hydro_Cyl_T'][:final_time_index]
if v: print("Done!")
if v: print("Computing W", end = " ... ")
W  = scalar_data['ScMAX']['hydro_Cyl_En'] + scalar_data['ScMAX']['hydro_Cyl_M0']
W += scalar_data['ScMAX']['hydro_Cyl_T']  - scalar_data['ScMAX']['hydro_Cyl_M']
W = W[:final_time_index]
if v: print("Done!")
if v: print("Computing beta", end = " ... ")
beta = T/W
if v: print("Done!")

if (not nd):
    plot_and_save(beta, "beta", q_data_folder, q_pics_folder)

# Compute quadrupole and save data file, keeping only the first part
if v: print("Computing Qxx", end = " ... ")
Qxx  = scalar_data['ScMAX']['hydro_Qxx'][:final_time_index]
if (not nd): np.savetxt(q_data_folder + "/Qxx.dat", Qxx)
if v: print("Done!")
if v: print("Computing Qyy", end = " ... ")
Qyy  = scalar_data['ScMAX']['hydro_Qyy'][:final_time_index]
if (not nd): np.savetxt(q_data_folder + "/Qyy.dat", Qyy)
if v: print("Done!")
if v: print("Computing Qxy", end = " ... ")
Qxy  = scalar_data['ScMAX']['hydro_Qxy'][:final_time_index]
if (not nd): np.savetxt(q_data_folder + "/Qxy.dat", Qxy)
if v: print("Done!")
if (not nd): np.savetxt(q_data_folder + "/beta.dat", beta)
if (not nd):
    if v: print("Created data files:", q_data_folder + "/Qxx.dat",  q_data_folder + "/Qyy.dat and",  q_data_folder + "/Qyx.dat",)

if (not nd): plot_and_save(Qxx, "Qxx", q_data_folder, q_pics_folder)
if (not nd): plot_and_save(Qyy, "Qyy", q_data_folder, q_pics_folder)
if (not nd): plot_and_save(Qxy, "Qxy", q_data_folder, q_pics_folder)


# Find the maximum of beta
if v: print("Finding tha maximum of beta", end = " ... ")

index_maximum_beta = np.argmax(beta)

if v: print("Done!")

if (index_maximum_beta == (len(beta) - 1)):
    print("\033[1mError:\033[0m the merger is not in the selected time-range!")
    sys.exit(1)

time_maximum_beta  = time[index_maximum_beta]

# Find all the maxima of the quadrupole
if (args.quadrupole == "Qxx" or args.quadrupole == None):
    q = Qxx
    args.quadrupole = "Qxx"
elif(args.quadrupole == "Qyy"):
    q = Qyy
elif(args.quadrupole == "Qxy"):
    q = Qxy

if v: print("Finding tha maxima of", args.quadrupole, end = " ... ")

maxima_q     = np.array([])
maxima_time  = np.array([])
indexes_max  = np.array([], dtype = np.int16)

# Time distance between the a maximum and the beta maximum
dt_beta = np.array([])

for i in range(len(q)-1):
    if ((q[i] > q[i+1]) and (q[i] > q[i-1])):
        maxima_q     = np.append(maxima_q, q[i])
        maxima_time  = np.append(maxima_time, time[i])
        dt_beta      = np.append(dt_beta, np.abs(time[i] - time_maximum_beta))
        indexes_max  = np.append(indexes_max, i)

if v: print("Done!")


# Find the starting point of the merger by finding the closes maximum
# of hplus to the maximum of beta

if v: print("Computing maxima for the running average", end = " ... ")

index_minimum_dt_beta = np.argmin(dt_beta)
time_zero             = maxima_time[index_minimum_dt_beta]


# Consider only the maxima after the start of the merger
indexes_maxima_after_zero = indexes_max[index_minimum_dt_beta:]

# Find the corresponding iterations, this variable contains the iterations
# corresponding to the maxima
iterations_maxima = [iterations[int(i)] for i in indexes_maxima_after_zero]

if v: print("Done!")

print("Merger found at time: {:.3f} ms".format(time_zero))

# Reading the vectors
descXY_file = os.path.join(h5_folder, sim_name + "_descXY.h5")
if v: print("Looking for descriptor XY h5 file:", descXY_file)
folder_2d   = sim_folder + "/o*-00??/*/data/H5_2d/*.xy.h5"

# Check if the file exists, if is there, read it
if (os.path.isfile(descXY_file)):
    print("Descriptor XY h5 file found! Reading it", end = " ... ")
    descXY = H5.CreateDescriptorList(folder_2d, descXY_file)
    print("Done!")
else:
    print("Descriptor XY h5 file not found! Producing it")
    descXY = H5.CreateDescriptorList(folder_2d, descXY_file)
    print("Done!")

# Maximum refinement level
max_rl = descXY[0]['rl'][-1]

if (not nd):
    print("Analyzing data, it takes quite some time")

# Reading the vectors
dataXY_file = os.path.join(h5_folder, sim_name + "_dataXY.h5")

# Iterations stored in the h5 files
iterations_h5   = descXY[0]['it']
iterations_h5   = iterations_h5[(iterations_h5 < iterations[-1])]
final_iteration = iterations_h5[-1]


def find_close(target, lst):
    """Return the closest element to target in lst"""
    dist = [np.abs(x - target) for x in lst]
    return lst[np.argmin(dist)]

# Fixing iterations max with the ones in h5
iterations_maxima_h5 = [find_close(x, iterations_h5) for x in iterations_maxima]

# Number of current iteration, starting from 1
i              = 1
i_final        = len(iterations_h5)
last_iteration = iterations_h5[-1]
len_str        = len(str(last_iteration))

if (not nd):
    if (not args.parallel):
        for ite in iterations_h5:
            # Print stat
            print("Status:", format(100*ite/last_iteration,'.3f'), "% completed!")
            if v: print("Iteration:", ite, "/", last_iteration)
            # Estimate remaining time
            try:
                seconds = extract_and_analyze_data(descXY, ite, len_str, sim_name)
                m, s = divmod(seconds*(i_final - i), 60)
                h, m = divmod(m, 60)
                print("Estimated remaining time: {:02d} hours, {:02d} minutes and {:02d} seconds".format(int(h),int(m),int(s)))
                i += 1
            except Exception as e:
                print("\033[1mWarning:\033[0m Iteration {} failed!".format(ite), e)
        print("Status: Complete!")
    else:
        # Try parallelism
        from joblib import Parallel, delayed
        import multiprocessing

        num_cores = multiprocessing.cpu_count()
        if v: print("Working with {} threads".format(num_cores))
        _ = Parallel(n_jobs = num_cores)(delayed(extract_and_analyze_data)(descXY, ite, len_str, sim_name) for ite in iterations_h5)


    if v: print("Producing animation", end = " ... ")
    # try:
    #     # result = subprocess.check_output("convert - delay 10 -loop 0 " + rl_pics_suffix + "/*.png " + sim_name + ".gif", shell = True)
    # except:
    #     print("\033[1mWarning:\033[0m Animation could not be produced")
    if v: print("Done")

# Compute averages

print("Performing averages")

# To use with zfill
len_iteration = len(str(iterations_maxima_h5[-1]))

# Prepare arrays for the fit
t  = np.array([])
a1 = np.array([])
b1 = np.array([])
a2 = np.array([])
b2 = np.array([])

for j in range(len(iterations_maxima_h5) - 1 - num_periods):

    print("Status:", format(100*j/(len(iterations_maxima_h5) - 1 - num_periods),'.3f'), "% completed!")
    if v: print("Index:", j + 1, "/", len(iterations_maxima_h5) - 1 - num_periods)
    iteration_init     = iterations_maxima_h5[j]
    iteration_finish   = iterations_maxima_h5[j + 1 + num_periods]
    iterations_between = [i for i in iterations_h5
                         if (i >= iteration_init and i <= iteration_finish)]
    time_init          = scalar_data_pp['t_ms'][(np.where(np.array(iterations) == iterations_maxima[j]))[0]][0]
    time_finish        = scalar_data_pp['t_ms'][(np.where(np.array(iterations) == iterations_maxima[j + 1 + num_periods]))[0]][0]
    time = 0.5*(time_init + time_finish) - time_zero
    print("Time after the merger: {:.3f} ms (init: {:.3f}, finish: {:.3f})".format(time, time_init - time_zero, time_finish - time_zero))

    # These arrays collect all the data for the current average
    surface_x  = np.array([])
    surface_y  = np.array([])

    fig, (ax, ax2) = plt.subplots(2)

    # Accumulate F(R) and Omega(R)
    dataXY = H5.CreateJoinedHDF5data(descXY, 0, max_rl, dataXY_file)
    dataX  = H5.Extract1dSlice(dataXY, size = 2)
    dataY  = H5.Extract1dSlice(dataXY, size = 2, dir = 1)
    # Ingore all points within a radius of 1
    zerox  = np.where(dataX['X[0]'] <= 1)[0][-1]
    zeroy  = np.where(dataY['X[1]'] <= 1)[0][-1]

    # I assume x and y are equivalent
    F_acc  = np.zeros(len(dataX['X[0]']) - zerox)
    Om_acc = np.zeros(len(dataX['X[0]']) - zerox)
    Bp_acc = np.zeros(len(dataX['X[0]']) - zerox)

    for i in iterations_between:
        # Read data
        # if v: print("Reading dataset corresponding to iteration", i, end = " ... ")
        dataXY = H5.CreateJoinedHDF5data(descXY, i, max_rl, dataXY_file)
        dataX  = H5.Extract1dSlice(dataXY, size = 2)
        dataY  = H5.Extract1dSlice(dataXY, size = 2, dir = 1)
        PP.PostProcessing3dFull(dataX, kind = "X")
        PP.PostProcessing3dFull(dataY, kind = "Y")

        F_acc  = np.vstack((F_acc, dataX['F'][zerox:]))
        F_acc  = np.vstack((F_acc, dataY['F'][zeroy:]))
        Om_acc = np.vstack((Om_acc, dataX['Omega'][zerox:]))
        Om_acc = np.vstack((Om_acc, dataY['Omega'][zeroy:]))
        Bp_acc = np.vstack((Bp_acc, -dataX['betaphi'][zerox:]))
        Bp_acc = np.vstack((Bp_acc, -dataY['betaphi'][zeroy:]))

        # if v: print("Done!")
        # print(np.shape(dataY['X[1]']))

        # Index of the center
        # if v: print("Reading F and Omega", end = " ... ")
        # zerox   = np.where(dataX['X[0]'] == 0)[0][-1]
        # zeroy   = np.where(dataY['X[1]'] == 0)[0][-1]

        # Separate F(Omega) in two branches before and after Fmax
        # F_xmax_ind  = np.argmax(dataX['F'][zerox:])
        # F_ymax_ind  = np.argmax(dataY['F'][zeroy:])

        F_xmax_ind  = np.argmax(dataX['Omega'][zerox:])
        F_ymax_ind  = np.argmax(dataY['Omega'][zeroy:])

        # if (dataX['X[0]'][zerox + F_xmax_ind] > 1):
        #     f_x1_tmp     = dataX['F'][zerox:(zerox + F_xmax_ind)]
        #     f_x2_tmp     = dataX['F'][(zerox + F_xmax_ind):]
        #     omega_x1_tmp = dataX['Omega'][zerox:(zerox + F_xmax_ind)]
        #     omega_x2_tmp = dataX['Omega'][(zerox + F_xmax_ind):]

        #     # Remove elements below cut_f
        #     # if v: print("Delete below the cut", end = " ... ")
        #     indexes        = np.where(np.abs(f_x1_tmp) < cut_f)
        #     f_x1_tmp       = np.delete(f_x1_tmp, indexes)
        #     omega_x1_tmp   = np.delete(omega_x1_tmp, indexes)

        #     # Remove elements below 0
        #     indexes        = np.where(f_x1_tmp < 0)
        #     f_x1_tmp       = np.delete(f_x1_tmp, indexes)
        #     omega_x1_tmp   = np.delete(omega_x1_tmp, indexes)
        #     # if v: print("Done!")

        #     # Remove elements below cut_f
        #     # if v: print("Delete below the cut", end = " ... ")
        #     indexes        = np.where(np.abs(f_x2_tmp) < 2*cut_f)
        #     f_x2_tmp       = np.delete(f_x2_tmp, indexes)
        #     omega_x2_tmp   = np.delete(omega_x2_tmp, indexes)

        #     # Remove elements below 0
        #     indexes        = np.where(f_x2_tmp < 0)
        #     f_x2_tmp       = np.delete(f_x2_tmp, indexes)
        #     omega_x2_tmp   = np.delete(omega_x2_tmp, indexes)
        #     # if v: print("Done!")

        tmp_surface_x = np.where(dataX['rho'][zerox:] >= dataX['rho'][zerox]*surface_density)[0][-1]
        surface_x = np.append(surface_x, tmp_surface_x)

        #     # Remove elements outside surface
        #     indexes        = np.where(omega_x2_tmp < dataX['Omega'][zerox + tmp_surface_x])
        #     f_x2_tmp       = np.delete(f_x2_tmp, indexes)
        #     omega_x2_tmp   = np.delete(omega_x2_tmp, indexes)
        #     # if v: print("Done!")

        #     ax.plot(omega_x1_tmp, f_x1_tmp, color = 'blue', alpha = 0.1)
        #     ax.plot(omega_x2_tmp, f_x2_tmp, color = 'green', alpha = 0.1)
        #     ax2.plot(dataX['X[0]'][zerox:(zerox + F_xmax_ind)],  dataX['Omega'][zerox:(zerox + F_xmax_ind)], color = 'blue', alpha = 0.1)
        #     ax2.plot(dataX['X[0]'][(zerox + F_xmax_ind):], dataX['Omega'][(zerox + F_xmax_ind):], color = 'green', alpha = 0.1)

        #     f_x1        = np.append(f_x1, f_x1_tmp)
        #     f_x2        = np.append(f_x2, f_x2_tmp)
        #     omega_x1 = np.append(omega_x1, omega_x1_tmp)
        #     omega_x2 = np.append(omega_x2, omega_x2_tmp)

        # if (dataX['X[1]'][zeroy + F_ymax_ind] > 1):
        #     f_y1_tmp     = dataY['F'][zeroy:(zeroy + F_ymax_ind)]
        #     f_y2_tmp     = dataY['F'][(zeroy + F_ymax_ind):]
        #     omega_y1_tmp = dataY['Omega'][zeroy:(zeroy + F_ymax_ind)]
        #     omega_y2_tmp = dataY['Omega'][(zeroy + F_ymax_ind):]

        #     # Remove elements below cut_f
        #     # if v: print("Delete below the cut", end = " ... ")
        #     indexes        = np.where(np.abs(f_y1_tmp) < cut_f)
        #     f_y1_tmp       = np.delete(f_y1_tmp, indexes)
        #     omega_y1_tmp   = np.delete(omega_y1_tmp, indexes)

        #     # Remove elements below 0
        #     indexes        = np.where(f_y1_tmp < 0)
        #     f_y1_tmp       = np.delete(f_y1_tmp, indexes)
        #     omega_y1_tmp   = np.delete(omega_y1_tmp, indexes)
        #     # if v: print("Done!")

        #     # Remove elements below cut_f
        #     # if v: print("Delete below the cut", end = " ... ")
        #     indexes        = np.where(np.abs(f_y2_tmp) < cut_f)
        #     f_y2_tmp       = np.delete(f_y2_tmp, indexes)
        #     omega_y2_tmp   = np.delete(omega_y2_tmp, indexes)

        #     # Remove elements below 0
        #     indexes        = np.where(f_y2_tmp < 0)
        #     f_y2_tmp       = np.delete(f_y2_tmp, indexes)
        #     omega_y2_tmp   = np.delete(omega_y2_tmp, indexes)
        #     # if v: print("Done!")

        tmp_surface_y = np.where(dataY['rho'][zeroy:] >= dataY['rho'][zeroy]*surface_density)[0][-1]
        surface_y = np.append(surface_y, tmp_surface_y)

        #     # Remove elements outside surface
        #     indexes        = np.where(omega_y2_tmp < dataY['Omega'][zeroy + tmp_surface_y])
        #     f_y2_tmp       = np.delete(f_y2_tmp, indexes)
        #     omega_y2_tmp   = np.delete(omega_y2_tmp, indexes)
        #     # if v: print("Done!")

        #     ax.plot(omega_y1_tmp, f_y1_tmp, color = 'red', alpha = 0.1)
        #     ax.plot(omega_y2_tmp, f_y2_tmp, color = 'yellow', alpha = 0.1)
        #     ax2.plot(dataY['X[1]'][zeroy:(zeroy + F_ymax_ind)],  dataY['Omega'][zeroy:(zeroy + F_ymax_ind)], color = 'red', alpha = 0.1)
        #     ax2.plot(dataY['X[1]'][(zeroy + F_ymax_ind):], dataY['Omega'][(zeroy + F_ymax_ind):], color = 'yellow', alpha = 0.1)

        #     f_y1        = np.append(f_y1, f_y1_tmp)
        #     f_y2        = np.append(f_y2, f_y2_tmp)
        #     omega_y1 = np.append(omega_y1, omega_y1_tmp)
        #     omega_y2 = np.append(omega_y2, omega_y2_tmp)

        # ax.plot(omega_x1_tmp, f_x1_tmp, color = 'blue', alpha = 0.1)
        # ax.plot(omega_x2_tmp, f_x2_tmp, color = 'green', alpha = 0.1)
        ax.plot(dataX['Omega'][zerox:(zerox + F_xmax_ind)], dataX['F'][zerox:(zerox + F_xmax_ind)], color = 'blue', alpha = 0.1)
        ax.plot(dataX['Omega'][(zerox + F_xmax_ind):], dataX['F'][(zerox + F_xmax_ind):], color = 'green', alpha = 0.1)
        ax2.plot(dataX['X[0]'][zerox:(zerox + F_xmax_ind)],  dataX['Omega'][zerox:(zerox + F_xmax_ind)], color = 'blue', alpha = 0.1)
        ax2.plot(dataX['X[0]'][(zerox + F_xmax_ind):], dataX['Omega'][(zerox + F_xmax_ind):], color = 'green', alpha = 0.1)
        ax.plot(dataY['Omega'][zeroy:(zeroy + F_ymax_ind)], dataY['F'][zeroy:(zeroy + F_ymax_ind)], color = 'red', alpha = 0.1)
        ax.plot(dataY['Omega'][(zeroy + F_ymax_ind):], dataY['F'][(zeroy + F_ymax_ind):], color = 'yellow', alpha = 0.1)
        ax2.plot(dataY['X[1]'][zeroy:(zeroy + F_ymax_ind)],  dataY['Omega'][zeroy:(zeroy + F_ymax_ind)], color = 'red', alpha = 0.1)
        ax2.plot(dataY['X[1]'][(zeroy + F_ymax_ind):], dataY['Omega'][(zeroy + F_ymax_ind):], color = 'yellow', alpha = 0.1)

    ax2.set_xlim([0,10])
    ax2.set_ylim([0,0.06])
    ax.set_ylim([0,10])
    ax.set_xlim([0,0.07])

    F_acc  = np.delete(F_acc, 0, axis = 0)
    Om_acc = np.delete(Om_acc, 0, axis = 0)
    Bp_acc = np.delete(Bp_acc, 0, axis = 0)

    average_surface = int(np.mean(np.append(surface_x, surface_y)))

    Om_averag = np.mean(Om_acc, axis = 0)
    Bp_averag = np.mean(Bp_acc, axis = 0)

    ax2.plot(dataX['X[0]'][zerox:]*CU_to_km, Bp_averag/CU_to_ms, 'black', lw = 2)
    ax2.plot(dataX['X[0]'][zerox:]*CU_to_km, Om_averag/CU_to_ms, 'red', lw = 2)

    F_acc  = np.delete(F_acc, np.s_[average_surface:len(F_acc[0])], axis = 1)
    Om_acc = np.delete(Om_acc, np.s_[average_surface:len(Om_acc[0])], axis = 1)

    F_averag  = np.mean(F_acc, axis = 0)
    Om_averag = np.mean(Om_acc, axis = 0)

    # print("Distance F_averag - Om_averag", np.argmax(F_averag) - np.argmax(Om_averag))

    ax.plot(Om_averag, F_averag, 'black', lw = 2)

    np.savetxt(rl_av_data_folder + "/" + str(iteration_init).zfill(len_iteration) + "_{:.3f}".format(time) + "_F_averag.dat", F_averag)
    np.savetxt(rl_av_data_folder + "/" + str(iteration_init).zfill(len_iteration) + "_{:.3f}".format(time) + "_Omega_averag.dat", Om_averag)

    sys.stdout = open(os.devnull, "w")
    tikz_save(rl_av_pics_folder + "/" +  str(iteration_init).zfill(len_iteration) + "_{:.3f}".format(time) + "_F_Omega.tikz", figureheight = '\\figureheight', figurewidth = '\\figurewidth')
    sys.stdout = sys.__stdout__

    # index_max = np.argmax(Om_averag)
    # om_high    = np.array(Om_averag[index_max:])
    # F_high    = np.array(F_averag[index_max:])
    # om_low    = np.array(Om_averag[:index_max])
    # F_low     = np.array(F_averag[:index_max])

    # def F_fun_high(omega, alpha, beta, gamma, delta, eta):
    #     return - (alpha*omega + gamma) + np.sqrt((alpha*omega + gamma)**2 - delta*omega - eta - beta*omega*omega)
    # def F_fun_low(omega, alpha, beta, gamma, delta, eta):
    #     return - (alpha*omega + gamma) - np.sqrt((alpha*omega + gamma)**2 - delta*omega - eta - beta*omega*omega)

    # # Linear free
    # H_lin = np.poly1d(np.polyfit(om_high, F_high, 1))
    # resi  = np.sum((F_high - H_lin(om_high))**2)
    # with open(rl_av_data_folder + "/H_lin_free.dat", 'a') as f:
    #     print(time, H_lin[0], H_lin[1], resi, file = f)

    # # # Parabolic U free
    # # # H_parU = curve_fit(parab, F_high, om_high)
    # # H_parU = np.poly1d(np.polyfit(F_high, om_high, 2))
    # # resi  = np.sum((om_high - H_parU(om_high))**2)
    # # with open(rl_av_data_folder + "/H_parU_free.dat", 'a') as f:
    # #     print(time, H_parU[0], H_parU[1], H_parU[2], resi, file = f)

    # # Parabolic V free
    # H_parV = np.poly1d(np.polyfit(om_high, F_high, 2))
    # resi  = np.sum((F_high - H_parV(om_high))**2)
    # with open(rl_av_data_folder + "/H_parV_free.dat", 'a') as f:
    #     print(time, H_parV[0], H_parV[1], H_parV[2], resi, file = f)

    # # Parabolic H free
    # try:
    #     F_high_fit = curve_fit(F_fun_high, om_high, F_high)
    #     resi  = np.sum((F_high - F_fun_high(om_high, *F_high_fit[0]))**2)
    #     with open(rl_av_data_folder + "/H_parH_free.dat", 'a') as f:
    #         print(time, F_high_fit[0][0], F_high_fit[0][1], F_high_fit[0][2],
    #               F_high_fit[0][3], F_high_fit[0][4], resi, file = f)
    # except Exception as e:
    #     print(str(e))

    # x_high = np.linspace(np.amin(om_high), np.amax(om_high), 100)
    # y_high = np.linspace(np.amin(F_high), np.amax(F_high), 100)
    # # ax.plot(x_high, H_lin(x_high), color = 'purple', ls = 'dashed', lw = 2)
    # # ax.plot(parab(y_high, *H_parU[0]), y_high, color = 'blue', ls = 'dashed', lw = 2)
    # # ax.plot(H_parU(y_high), y_high, color = 'blue', ls = 'dashed', lw = 2)
    # # ax.plot(x_high, H_parV(x_high), color = 'green', ls = 'dashed', lw = 2)
    # ax.plot(x_high, F_fun_high(x_high, *F_high_fit[0]), color = 'red', ls = 'dashed', lw = 2)

    # if (not len(om_low) < 5):
    #     # Linear free
    #     L_lin = np.poly1d(np.polyfit(om_low, F_low, 1))
    #     resi  = np.sum((F_low - L_lin(om_low))**2)
    #     with open(rl_av_data_folder + "/L_lin_free.dat", 'a') as f:
    #         print(time, L_lin[0], L_lin[1], resi, file = f)

    #     # Parabolic U free
    #     L_parU = np.poly1d(np.polyfit(F_low, om_low, 2))
    #     resi  = np.sum((om_low - L_parU(F_low))**2)
    #     with open(rl_av_data_folder + "/L_parU_free.dat", 'a') as f:
    #         print(time, L_parU[0], L_parU[1], L_parU[2], resi, file = f)

    #     # Parabolic V free
    #     L_parV = np.poly1d(np.polyfit(om_low, F_low, 2))
    #     resi  = np.sum((F_low - L_parV(om_low))**2)
    #     with open(rl_av_data_folder + "/L_parV_free.dat", 'a') as f:
    #         print(time, L_parV[0], L_parV[1], L_parV[2], resi, file = f)

    #     # Parabolic L free
    #     F_low_fit = curve_fit(F_fun_low, om_low, F_low)
    #     resi  = np.sum((F_low - F_fun_low(om_low, *F_low_fit[0]))**2)
    #     with open(rl_av_data_folder + "/L_parL_free.dat", 'a') as f:
    #         print(time, F_low_fit[0][0], F_low_fit[0][1], F_low_fit[0][2],
    #               F_low_fit[0][3], F_low_fit[0][4], resi, file = f)

    #     x_low  = np.linspace(np.amin(om_low), np.amax(om_low), 100)
    #     y_low = np.linspace(np.amin(F_low), np.amax(F_low), 100)
    #     # ax.plot(x_low, L_lin(x_low), color = 'purple', ls = 'dashed', lw = 2)
    #     # ax.plot(parab(y_low, *L_parU[0]), y_low, color = 'blue', ls = 'dashed', lw = 2)
    #     # ax.plot(x_low, L_parV(x_low), color = 'green', ls = 'dashed', lw = 2)
    #     ax.plot(x_low, F_fun_low(x_low, *F_low_fit[0]), color = 'red', ls = 'dashed', lw = 2)

    # # Put a very high weight on the matching point
    # if (not len(om_low) < 5):
    #     weights_L     = np.ones(len(om_low))
    #     weights_L[-1] = 100

    # weights_H     = np.ones(len(om_high))
    # weights_H[0]  = 100

    # # Linear free
    # H_linC = np.poly1d(np.polyfit(om_high, F_high, 1, w = weights_H))
    # resi  = np.sum((F_high - H_lin(om_high))**2)
    # with open(rl_av_data_folder + "/H_lin_con.dat", 'a') as f:
    #     print(time, H_linC[0], H_linC[1], resi, file = f)

    # # # Parabolic U free
    # # H_parUC = np.poly1d(np.polyfit(F_high, F_high, 1, w = weights_H))
    # # resi  = np.sum((F_high - parab(om_high, *H_parUC[0]))**2)
    # # with open(rl_av_data_folder + "/H_parU_con.dat", 'a') as f:
    # #     print(time, H_parU[0], H_parU[1], H_parU[0], resi, file = f)

    # # Parabolic V free
    # H_parVC = np.poly1d(np.polyfit(om_high, F_high, 2, w = weights_H))
    # resi  = np.sum((F_high - H_lin(om_high))**2)
    # with open(rl_av_data_folder + "/H_parV_con.dat", 'a') as f:
    #     print(time, H_parVC[0], H_parVC[1], H_parVC[0], resi, file = f)

    # # Parabolic H free
    # try:
    #     F_high_fitC = curve_fit(F_fun_high, om_high, F_high, sigma = 1/weights_H)
    #     resi  = np.sum((F_high - F_fun_high(om_high, *F_high_fitC[0]))**2)
    #     with open(rl_av_data_folder + "/H_parH_con.dat", 'a') as f:
    #         print(time, F_high_fitC[0][0], F_high_fitC[0][1], F_high_fitC[0][2],
    #               F_high_fitC[0][3], F_high_fitC[0][4], resi, file = f)
    # except Exception as e:
    #     print(str(e))

    # x_high = np.linspace(np.amin(om_high), np.amax(om_high), 100)
    # y_high = np.linspace(np.amin(F_high), np.amax(F_high), 100)
    # # ax.plot(x_high, H_linC(x_high), color = 'purple', ls = 'dotted', lw = 2)
    # # ax.plot(parab(y_high, *H_parUC[0]), y_high, color = 'blue', ls = 'dotted', lw = 2)
    # # ax.plot(x_high, H_parVC(x_high), color = 'green', ls = 'dotted', lw = 2)
    # ax.plot(x_high, F_fun_high(x_high, *F_high_fitC[0]), color = 'red', ls = 'dotted', lw = 2)

    # if (not len(om_low) < 5):
    #     # Linear free
    #     L_linC = np.poly1d(np.polyfit(om_low, F_low, 1, w = weights_L))
    #     resi  = np.sum((F_low - L_lin(om_low))**2)
    #     with open(rl_av_data_folder + "/L_lin_con.dat", 'a') as f:
    #         print(time, L_linC[0], L_linC[1], resi, file = f)

    #     # Parabolic U free
    #     L_parUC = np.poly1d(np.polyfit(F_low, om_low, 1, w = weights_L))
    #     resi  = np.sum((om_low - L_parUC(F_low))**2)
    #     with open(rl_av_data_folder + "/L_parU_con.dat", 'a') as f:
    #         print(time, L_parU[0], L_parU[1], L_parU[2], resi, file = f)

    #     # Parabolic V free
    #     L_parVC = np.poly1d(np.polyfit(om_low, F_low, 2, w = weights_L))
    #     resi  = np.sum((F_low - L_lin(om_low))**2)
    #     with open(rl_av_data_folder + "/L_parV_con.dat", 'a') as f:
    #         print(time, L_parVC[0], L_parVC[1], L_parVC[0], resi, file = f)

    #     # Parabolic L free
    #     try:
    #         F_low_fitC = curve_fit(F_fun_low, om_low, F_low, sigma = 1/weights_L)
    #         resi  = np.sum((F_low - F_fun_low(om_low, *F_low_fitC[0]))**2)
    #         with open(rl_av_data_folder + "/L_parL_con.dat", 'a') as f:
    #             print(time, F_low_fitC[0][0], F_low_fitC[0][1], F_low_fitC[0][2],
    #                   F_low_fitC[0][3], F_low_fitC[0][4], resi, file = f)
    #     except Exception as e:
    #         print(str(e))

    #     x_low = np.linspace(np.amin(om_low), np.amax(om_low), 100)
    #     y_low = np.linspace(np.amin(F_low), np.amax(F_low), 100)
    #     # ax.plot(x_low, L_linC(x_low), color = 'purple', ls = 'dotted', lw = 2)
    #     # ax.plot(L_parUC(y_low), y_low, color = 'purple', ls = 'dotted', lw = 2)
    #     # ax.plot(x_low, L_parVC(x_low), color = 'green', ls = 'dotted', lw = 2)
    #     ax.plot(x_low, F_fun_low(x_low, *F_low_fitC[0]), color = 'red', ls = 'dotted', lw = 2)


    # def F_fun_low(omega, alpha, beta, gamma, delta, eta):
    #     return - (alpha*omega + gamma) - np.sqrt((alpha*omega + gamma)**2 - delta*omega - eta - beta*omega*omega)

    # def F_fun_high(omega, alpha, beta, gamma, delta, eta):
    #     return - (alpha*omega + gamma) + np.sqrt((alpha*omega + gamma)**2 - delta*omega - eta - beta*omega*omega)

    # # Linear fit
    # # Separate in two parts
    # index_max = np.argmax(Om_averag)
    # try:
    #     om_high    = np.array(Om_averag[index_max:])
    #     F_high    = np.array(F_averag[index_max:])
    #     poly_high = np.poly1d(np.polyfit(om_high, F_high, 1))
    #     print("High", poly_high)
    #     x_high = np.linspace(np.amin(om_high), np.amax(om_high), 100)
    #     ax.plot(x_high, poly_high(x_high), 'orange', lw = 2)
    #     F_high_fit = curve_fit(F_fun_high, om_high, F_high)
    #     print(F_high_fit[0])
    #     ax.plot(x_high, F_fun_high(x_high, *F_high_fit[0]), color = 'red', ls = 'dashed', lw = 2)
    # except:
    #     pass
    # try:
    #     om_low    = np.array(Om_averag[:index_max])
    #     F_low     = np.array(F_averag[:index_max])
    #     poly_low  = np.poly1d(np.polyfit(F_low, om_low, 2))
    #     # print("Low", poly_low)
    #     y_low  = np.linspace(np.amin(F_low), np.amax(F_low), 100)
    #     ax.plot(poly_low(y_low), y_low, 'orange', lw = 2)

    #     x_low  = np.linspace(np.amin(om_low), np.amax(om_low), 100)
    #     F_low_fit = curve_fit(F_fun_low, om_low, F_low, p0 = F_high_fit[0])
    #     print(F_low_fit[0])
    #     ax.plot(x_low, F_fun_low(x_low, *F_low_fit[0]), color = 'red', ls = 'dashed', lw = 2)
    # except:
    #     pass

    # def sq(x):
    #     return np.abs(np.sum(F_averag*F_averag) +
    #                   np.sum(x[0]*F_averag*Om_averag) + np.sum(x[1]*F_averag) +
    #                   np.sum(x[2]*Om_averag*Om_averag) + np.sum(x[3]*Om_averag) + x[4])

    # trial_point = np.array(F_high_fit[0])

    # res = minimize(sq, trial_point)
    # print(res.x)
    # try:
    #     ax3.plot(x_low, F_fun_low(x_low, res.x[0], res.x[1], res.x[2], res.x[3],
    #                              res.x[4]), color = 'red', ls = 'dashed', lw = 2)
    # except:
    #     pass
    # ax3.plot(x_high, F_fun_high(x_high, res.x[0], res.x[1], res.x[2], res.x[3],
                             # res.x[4]), color = 'red', ls = 'dashed', lw = 2)

    # try:
    #     f_mean = np.average(np.vstack((F_low_fit[0], F_high_fit[0])), axis = 0,
    #                         weights = [index_max, len(F_averag) - index_max])
    #     print(f_mean)
    #     ax.plot(x_low, F_fun_low(x_low, *f_mean), color = 'green', ls = 'dashed', lw = 2)
    #     ax.plot(x_high, F_fun_high(x_high, *f_mean), color = 'green', ls = 'dashed', lw = 2)
    # except Exception as e:
    #     print("EXCEPTION: " + str(e))


    # # Finer average
    # F_weights  = (F_acc - F_averag)**(-2)
    # Om_weights = (Om_acc - Om_averag)**(-2)

    # F_averag_w   = np.average(F_acc, axis = 1, weights = F_weights)
    # Om_averag_w  = np.average(Om_acc, axis = 1, weights = Om_weights)

    # ax.plot(Om_averag_w, F_averag_w, 'red', lw = 2, ls = 'dashed')

    # np.savetxt(rl_av_data_folder + "/" + str(iteration_init).zfill(len_iteration) + "_{:.3f}".format(time) + "_disc_F1.dat", discretized_F1)
    # np.savetxt(rl_av_data_folder + "/" + str(iteration_init).zfill(len_iteration) + "_{:.3f}".format(time) + "_disc_Omega1.dat", discretized_Omega1)

#     # # Swap axis
#     # X = discretized_F
#     # Y = discretized_Omega

#     # poly =  np.polyfit(X, Y, 2)
#     # p = np.poly1d(poly)
#     # xj = np.linspace(np.amin(X), np.amax(X), 100)
#     # yj = p(xj)

#     # # Find the maximum
#     # crit      = p.deriv().r
#     # F_max     = [crit[crit.imag == 0].real][0][0]
#     # Omega_max = p(F_max)
#     # Omega_c   = p(0)

#     # ax.plot(discretized_Omega, discretized_F, color = 'black', lw = 2)
#     # ax.plot(yj, xj, 'orange', lw = 2)
#     # # ax.set_xlim([0.02,0.06])
#     # # ax.set_ylim([0,0.05])
#     # # ax.text(0.04, 0.04, "T = {:.3f} ms".format(time))
#     # fig.savefig(rl_av_pics_folder + "/{}_{:.3}".format(iteration_init, time) + ".png")
#     # plt.close(fig)

#     # om1 = np.linspace(np.amin(Omega1), np.amax(Omega1), 100)
#     # om2 = np.linspace(average_surface, np.amax(Omega2), 100)
#     # bis = np.linspace(np.amin(Omega1), np.amax(Omega2), 100)

#     # ax.plot(om1, f1_poly(om1), 'orange', lw = 2)
#     # ax.plot(om2, f2_poly(om2), 'orange', lw = 2)
#     # ax.plot(bis, bis_poly(bis), 'red', lw = 2)

#     # om1_rot = np.cos(final_angle)*om1 - np.sin(final_angle)*f1_poly(om1)
#     # f1_rot  = np.sin(final_angle)*om1 + np.cos(final_angle)*f1_poly(om1)
#     # om2_rot = np.cos(final_angle)*om2 - np.sin(final_angle)*f2_poly(om2)
#     # f2_rot  = np.sin(final_angle)*om2 + np.cos(final_angle)*f2_poly(om2)
#     # bis_rot = np.cos(final_angle)*bis - np.sin(final_angle)*bis_poly(bis)
#     # biF_rot = np.sin(final_angle)*bis + np.cos(final_angle)*bis_poly(bis)

#     # ax3.plot(om1_rot, f1_rot, 'orange', lw = 2)
#     # ax3.plot(om2_rot, f2_rot, 'orange', lw = 2)
#     # ax3.plot(bis_rot, biF_rot, 'red', lw = 2)

#     # ax.set_xlim([0.02,0.06])
#     # ax.set_ylim([0,0.05])
    ax.text(0.005, 1, "T = {:.3f} ms".format(time))
    fig.savefig(rl_av_pics_folder + "/{}_{:.3}".format(iteration_init, time) + ".png")
    plt.close(fig)

#     if v: print("Done!")

#     with open(rl_av_data_folder + "/fit_details.dat", 'a') as f:
#         print(time, f1_poly[0], f1_poly[1], f2_poly[0], f1_poly[1], file = f)

#     t   = np.append(t, time)
#     # Bsq is defined with a minus sign
#     a1 = np.append(a1, f1_poly[0])
#     b1 = np.append(b1, f1_poly[1])
#     a2 = np.append(a2, f2_poly[0])
#     b2 = np.append(b2, f2_poly[1])

# print("Status: Complete!")

# # if v: print("Producing animation", end = " ... ")
# # # result = subprocess.check_output("convert - delay 10 -loop 0 " + rl_av_pics_suffix + "/*.png " + sim_name + ".gif", shell = True)
# # if v: print("Done")

# print("Extrapolating model", end = " ... ")

# fig, (ax1, ax2, ax3, ax4) = plt.subplots(4, sharex = True)

# ax1.plot(t, a1)
# ax1.set_ylabel("$a_1(t)$")
# ax2.plot(t, b1)
# ax2.set_ylabel("$b_1(t)$")
# ax3.plot(t, a2)
# ax3.set_ylabel("$a_2(t)$")
# ax4.plot(t, b2)
# ax4.set_ylabel("$b_2(t)$")
# ax4.set_xlabel("$t[ms]$")

# # # Fit Omega_c

# # omega_c_fit = curve_fit(omega_c_try, t, c)
# # ax3.plot(t, omega_c_try(t, *omega_c_fit[0]), color = 'black', ls = 'dashed')

# # # Extract error bars

# # if (args.error_bars):

# #     xp = t
# #     yp = omega_c_try(t, *omega_c_fit[0])

# #     sigma = [y*0.1 for y in yp]
# #     ypp = [x + y for x, y in zip(yp, sigma)]
# #     ypm = [x - y for x, y in zip(yp, sigma)]
# #     ypp2 = [x + 2*y for x, y in zip(yp, sigma)]
# #     ypm2 = [x - 2*y for x, y in zip(yp, sigma)]
# #     ypp3 = [x + 3*y for x, y in zip(yp, sigma)]
# #     ypm3 = [x - 3*y for x, y in zip(yp, sigma)]
# #     ax3.fill_between(xp, ypp, ypm, alpha=.55, facecolor = 'orange', label = '\\SI{30}{\\percent}')
# #     ax3.fill_between(xp, ypp2, ypm2, alpha=.35, facecolor = 'orange', label = '\\SI{20}{\\percent}')
# #     ax3.fill_between(xp, ypp3, ypm3, alpha=.15, facecolor = 'orange', label = '\\SI{10}{\\percent}')


# # # Fit b

# # b_fit = curve_fit(b_try, t, b)

# # ax2.plot(t, b_try(t, *b_fit[0]), color = 'black', ls = 'dashed')

# # # Error bars

# # if (args.error_bars):

# #     xp = t
# #     yp = b_try(t, *b_fit[0])

# #     sigma = [y*0.1 for y in yp]
# #     ypp = [x + y for x, y in zip(yp, sigma)]
# #     ypm = [x - y for x, y in zip(yp, sigma)]
# #     ypp2 = [x + 2*y for x, y in zip(yp, sigma)]
# #     ypm2 = [x - 2*y for x, y in zip(yp, sigma)]
# #     ypp3 = [x + 3*y for x, y in zip(yp, sigma)]
# #     ypm3 = [x - 3*y for x, y in zip(yp, sigma)]
# #     ax2.fill_between(xp, ypp, ypm, alpha=.55, facecolor = 'orange')
# #     ax2.fill_between(xp, ypp2, ypm2, alpha=.35, facecolor = 'orange')
# #     ax2.fill_between(xp, ypp3, ypm3, alpha=.15, facecolor = 'orange')


# # # Fit B^2

# # Bsq_fit = curve_fit(Bsq_try, t, Bsq)

# # ax1.plot(t, Bsq_try(t, *Bsq_fit[0]), color = 'black', ls = 'dashed')

# # if (args.error_bars):

# #     xp = t
# #     yp = Bsq_try(t, *Bsq_fit[0])
# #     sigma = [y*0.1 for y in yp]

# #     ypp = [x + y for x, y in zip(yp, sigma)]
# #     ypm = [x - y for x, y in zip(yp, sigma)]
# #     ypp2 = [x + 2*y for x, y in zip(yp, sigma)]
# #     ypm2 = [x - 2*y for x, y in zip(yp, sigma)]
# #     ypp3 = [x + 3*y for x, y in zip(yp, sigma)]
# #     ypm3 = [x - 3*y for x, y in zip(yp, sigma)]
# #     ax1.fill_between(xp, ypp, ypm, alpha=.55, facecolor = 'orange')
# #     ax1.fill_between(xp, ypp2, ypm2, alpha=.35, facecolor = 'orange')
# #     ax1.fill_between(xp, ypp3, ypm3, alpha=.15, facecolor = 'orange')

# sys.stdout = open(os.devnull, "w")
# tikz_save(rl_av_pics_folder + "/" + sim_name + ".tikz", figureheight = '\\figureheight', figurewidth = '\\figurewidth')
# sys.stdout = sys.__stdout__

# fig.savefig(rl_av_pics_folder + "/" + sim_name + ".pdf", format = 'pdf', dpi = 1200)

# plt.close(fig)

# print("Done!")
# # print("Coefficients of the model:")
# # print("B^2(t)", *Bsq_fit[0])
# # # print('\n'.join('{}: {}'.format(*k) for k in enumerate(*Bsq_fit[0])))
# # print("b(t)", *b_fit[0])
# # # print('\n'.join('{}: {}'.format(*k) for k in enumerate(*b_fit[0])))
# # print("Omega_c(t)", *omega_c_fit[0])
# # # print('\n'.join('{}: {}'.format(*k) for k in enumerate(*omega_c_fit[0])))

# # print("The last bit of magic")

# # for j in range(len(iterations_maxima_h5) - 1 - num_periods):
# #     print(format(100*j/(len(iterations_maxima_h5) - 1 - num_periods),'.3f'), "%")

# #     fig = plt.figure()
# #     ax1 = plt.subplot2grid((3,2), (0,0))
# #     ax2 = plt.subplot2grid((3,2), (1,0), sharex = ax1)
# #     ax3 = plt.subplot2grid((3,2), (2,0), sharex = ax1)
# #     ax4 = plt.subplot2grid((3,2), (0,1), rowspan = 3)

# #     ax1.plot(t, Bsq, color = 'orange')
# #     ax1.set_ylabel("$B^2(t)$")
# #     ax1.minorticks_on()
# #     ax1.grid(True, which='major', linestyle='--')
# #     ax1.grid(True, which='minor', linewidth = 0.15, linestyle='--')
# #     ax1.get_yaxis().set_label_coords(-0.4,0.5)
# #     ax1.yaxis.set_major_formatter(mtick.FormatStrFormatter('%.5f'))
# #     # ax1.set_xlabel("$t~[ms]$")
# #     ax2.plot(t, b, color = 'orange')
# #     ax2.set_ylabel("$b(t)$")
# #     ax2.minorticks_on()
# #     ax2.grid(True, which='major', linestyle='--')
# #     ax2.grid(True, which='minor', linewidth = 0.15, linestyle='--')
# #     ax2.get_yaxis().set_label_coords(-0.4,0.5)
# #     ax2.yaxis.set_major_formatter(mtick.FormatStrFormatter('%.5f'))
# #     # ax2.set_xlabel("$t~[ms]$")
# #     ax3.plot(t, c, color = 'orange')
# #     ax3.set_ylabel("$\Omega_c(t)~[CU]$")
# #     ax3.set_xlabel("$t~[ms]$")
# #     ax3.minorticks_on()
# #     ax3.grid(True, which='major', linestyle='--')
# #     ax3.grid(True, which='minor', linewidth = 0.15, linestyle='--')
# #     ax3.get_yaxis().set_label_coords(-0.4,0.5)
# #     ax3.yaxis.set_major_formatter(mtick.FormatStrFormatter('%.5f'))

# #     plt.setp(ax1.get_xticklabels(), visible=False)
# #     plt.setp(ax2.get_xticklabels(), visible=False)

# #     ax3.plot(t, omega_c_try(t, *omega_c_fit[0]), color = 'black')
# #     ax2.plot(t, b_try(t, *b_fit[0]), color = 'black')
# #     ax1.plot(t, Bsq_try(t, *Bsq_fit[0]), color = 'black')

# #     iteration_init     = iterations_maxima_h5[j]
# #     iteration_finish   = iterations_maxima_h5[j + 1 + num_periods]
# #     iterations_between = [i for i in iterations_h5
# #                          if (i >= iteration_init and i <= iteration_finish)]
# #     time_init          = scalar_data_pp['t_ms'][(np.where(np.array(iterations) == iterations_maxima[j]))[0]][0]
# #     time_finish        = scalar_data_pp['t_ms'][(np.where(np.array(iterations) == iterations_maxima[j + 1 + num_periods]))[0]][0]
# #     time = 0.5*(time_init + time_finish) - time_zero

# #     # Vertical line for the time

# #     ax3.axvline(x = time, color = 'black')
# #     ax2.axvline(x = time, color = 'black')
# #     ax1.axvline(x = time, color = 'black')

# #     # These arrays collect all the data for the current average
# #     omega_x   = np.array([])
# #     f_x       = np.array([])
# #     omega_y   = np.array([])
# #     f_y       = np.array([])
# #     surface_x = np.array([])
# #     surface_y = np.array([])

# #     for i in iterations_between:
# #         # Read data
# #         # if v: print("Reading dataset corresponding to iteration", i, end = " ... ")
# #         dataXY = H5.CreateJoinedHDF5data(descXY, i, max_rl, dataXY_file)
# #         dataX  = H5.Extract1dSlice(dataXY, size = 2)
# #         dataY  = H5.Extract1dSlice(dataXY, size = 2, dir = 1)
# #         PP.PostProcessing3dFull(dataX, kind = "X")
# #         PP.PostProcessing3dFull(dataY, kind = "Y")
# #         # if v: print("Done!")

# #         # Index of the center
# #         # if v: print("Reading F and Omega", end = " ... ")
# #         zerox   = np.where(dataX['X[0]'] == 0)[0][-1]
# #         zeroy   = np.where(dataY['X[1]'] == 0)[0][-1]
# #         omega_x = np.append(omega_x, dataX['Omega'][zerox:])
# #         omega_y = np.append(omega_y, dataY['Omega'][zeroy:])
# #         f_x     = np.append(f_x, dataX['F'][zerox:])
# #         f_y     = np.append(f_y, dataY['F'][zeroy:])
# #         # if v: print("Done!")

# #         # Surface is the index of the last element with rho > than 0.01 rho_central
# #         # if v: print("Finding surface with density", surface_density, "rho_c", end = " ... ")
# #         tmp_surface_x = np.where(dataX['rho'][zerox:] >= dataX['rho'][zerox]*surface_density)[0][-1]
# #         tmp_surface_y = np.where(dataY['rho'][zeroy:] >= dataY['rho'][zeroy]*surface_density)[0][-1]
# #         surface_x = np.append(surface_x, omega_x[tmp_surface_x])
# #         surface_y = np.append(surface_y, omega_y[tmp_surface_y])
# #         # if v: print("Done!")

# #     F     = np.append(f_x, f_y)
# #     Omega = np.append(omega_x, omega_y)

# #     # Remove elements below cut_f
# #     # if v: print("Delete below the cut", end = " ... ")
# #     indexes = np.where(np.abs(F) < cut_f)
# #     F       = np.delete(F, indexes)
# #     Omega   = np.delete(Omega, indexes)

# #     # Remove elements below 0
# #     indexes = np.where(F < 0)
# #     F       = np.delete(F, indexes)
# #     Omega   = np.delete(Omega, indexes)
# #     # if v: print("Done!")

# #     # Extract average
# #     # if v: print("Performing average", end = " ... ")

# #     average_surface = np.mean(np.append(surface_x, surface_y))

# #     discretized_F     = np.linspace(0, np.amax(F), num_intervals)
# #     step_F            = discretized_F[1] - discretized_F[0]
# #     discretized_Omega = np.zeros(num_intervals)

# #     # Consider only Omegas whose F is between F[i] and F[i+1]
# #     for i in range(num_intervals-1):
# #         # indexes of omega that satisfy the condition
# #         omega_temp = Omega[(F > discretized_F[i]) & (F < discretized_F[i+1])]
# #         if (len(omega_temp) == 0):
# #             # Mark all those element which are invalid
# #             discretized_Omega[i] = -99999
# #         else:
# #             discretized_Omega[i] = np.mean(omega_temp)

# #     # Stagger F so that now the intervals are centered
# #     discretized_F = discretized_F + 0.5*step_F
# #     # if v: print("Done!")

# #     # Eliminate marked points
# #     indexes           = np.where(discretized_Omega < 0)
# #     discretized_Omega = np.delete(discretized_Omega, indexes)
# #     discretized_F     = np.delete(discretized_F, indexes)

# #     # Find the maximum
# #     index_max = np.argmax(discretized_Omega)
# #     omega_max = discretized_Omega[index_max]
# #     F_max     = discretized_F[index_max]

# #     # Eliminate points outside the surface marking them
# #     for i in range(len(discretized_Omega)):
# #         if(discretized_F[i] > F_max and discretized_Omega[i] < average_surface):
# #             discretized_F[i]     = -99999
# #             discretized_Omega[i] = -99999


# #     # Eliminate them
# #     indexes           = np.where(discretized_Omega < 0)
# #     discretized_Omega = np.delete(discretized_Omega, indexes)
# #     discretized_F     = np.delete(discretized_F, indexes)

# #     # omega_aver = omega_aver[:-1]
# #     # lin = lin[:-1]

# #     # Swap axis
# #     X = discretized_F
# #     Y = discretized_Omega

# #     poly =  np.polyfit(X, Y, 2)
# #     p = np.poly1d(poly)
# #     xj = np.linspace(np.amin(X), np.amax(X), 100)
# #     yj = p(xj)

# #     # Find the maximum
# #     crit      = p.deriv().r
# #     F_max     = [crit[crit.imag == 0].real][0][0]
# #     Omega_max = p(F_max)
# #     Omega_c   = p(0)

# #     ax4.plot(discretized_Omega, discretized_F, color = 'orange', lw = 2)
# #     # ax4.plot(yj, xj, 'green', lw = 2)
# #     omegaj = omega(time, xj)
# #     ax4.plot(omegaj, xj, color = 'black', lw = 2)
# #     ax4.set_xlim([0.02,0.06])
# #     ax4.set_ylim([0,100])
# #     ax4.text(0.03, 81, "T = {:.3f} ms".format(time))
# #     ax4.minorticks_on()
# #     ax4.grid(True, which='major', linestyle='--')
# #     ax4.grid(True, which='minor', linewidth = 0.15, linestyle='--')
# #     ax4.set_ylabel("$F(\Omega)~[CU]$")
# #     ax4.set_xlabel("$\Omega~[CU]$")

# #     fig.tight_layout()
# #     fig.subplots_adjust(hspace=0)

# #     fig.savefig(model_folder + "/{}_{:.3}".format(iteration_init, time) + ".png", dpi = 600, transparent=True)

# #     plt.close(fig)

# # print("Done!")
# # # try:
# # #     # result = subprocess.check_output("convert - delay 25 -loop 0 " + rl_av_pics_suffix + "/model/*.png " + sim_name + ".gif", shell = True)
# # # except:
# # #     print("\033[1mWarning:\033[0m Animation could not be produced")
# # print("Created the animation", sim_name + ".gif")

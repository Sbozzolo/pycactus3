#!/usr/bin/env python3

# Extract the rotation law from a simulation

# Author: Gabriele Bozzola (sbozzolo)
# Email: sbozzolator@gmail.com
# Version: 1.0
# First Stable: 27/08/17
# Last Edit: 29/08/17

import argparse
import sys
import os
import shutil
import time
import datetime
import re
import glob
from timeit import default_timer as timer
from datetime import timedelta as timedelta
import subprocess
from scipy.optimize import curve_fit
from scipy.optimize import minimize

import numpy as np
import matplotlib as mlp
mlp.use('Agg')


# Include PyCactus3
sys.path.append('/home/kepler/bozzola/PyCactus3')
import CarpetASCIII as CARPET
import CarpetH5     as H5
import UtilityPostProcessing as PP

# Parse command line arguments

parser = argparse.ArgumentParser()

parser.add_argument("-i", "--input", type = str, required = True,
                    help = "set simulation folder")
parser.add_argument("-s", "--short", action = "store_true",
                    help = "short output (in ms)")
parser.add_argument("-q", "--quadrupole", type = str,
                    help = "which quadrupole use?", choices = ["Qxx", "Qyy", "Qxy"])
parser.add_argument('--version', action='version', version='%(prog)s 1.0')

if len(sys.argv) == 1:
    parser.print_help()
    sys.exit(1)

args = parser.parse_args()

sim_folder        = args.input
sim_name          = sim_folder.split('/')[-1]
out_folder        = os.path.join(os.getcwd(), sim_name)
h5_folder         = os.path.join(out_folder, "h5")
q_data_folder     = os.path.join(out_folder, "q_data")

# Check if simulation folder exists
if (not os.path.isdir(sim_folder)):
    print("Simulation folder", sim_folder, "not existing!")
    sys.exit(1)

if (not args.short): print("Working on simulation:", sim_name)

# Reading the scalars
h5_scalar_file = os.path.join(h5_folder, sim_name + "_scalar.h5")

iterations = np.loadtxt(q_data_folder + "/iterations.dat")
time       = np.loadtxt(q_data_folder + "/time.dat")
beta       = np.loadtxt(q_data_folder + "/beta.dat")
Qxx        = np.loadtxt(q_data_folder + "/Qxx.dat")
Qyy        = np.loadtxt(q_data_folder + "/Qyy.dat")
Qxy        = np.loadtxt(q_data_folder + "/Qxy.dat")

index_maximum_beta = np.argmax(beta)
time_maximum_beta  = time[index_maximum_beta]

# Find all the maxima of the quadrupole
if (args.quadrupole == "Qxx" or args.quadrupole == None):
    q = Qxx
    args.quadrupole = "Qxx"
elif(args.quadrupole == "Qyy"):
    q = Qyy
elif(args.quadrupole == "Qxy"):
    q = Qxy

maxima_q     = np.array([])
maxima_time  = np.array([])
indexes_max  = np.array([], dtype = np.int16)

# Time distance between the a maximum and the beta maximum
dt_beta = np.array([])

for i in range(len(q)-1):
    if ((q[i] > q[i+1]) and (q[i] > q[i-1])):
        maxima_q     = np.append(maxima_q, q[i])
        maxima_time  = np.append(maxima_time, time[i])
        dt_beta      = np.append(dt_beta, np.abs(time[i] - time_maximum_beta))
        indexes_max  = np.append(indexes_max, i)

index_minimum_dt_beta = np.argmin(dt_beta)
time_zero             = maxima_time[index_minimum_dt_beta]

if (not args.short):
    print("Merger found at time: {:.3f} ms".format(time_zero))
else:
    print("{:.5f}".format(time_zero))

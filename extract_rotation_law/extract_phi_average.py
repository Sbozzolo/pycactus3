#!/usr/bin/env python3

# Extract the rotation law from a simulation

# Author: Gabriele Bozzola (sbozzolo)
# Email: sbozzolator@gmail.com
# Version: 1.0
# First Stable: 27/08/17
# Last Edit: 29/08/17

import argparse
import sys
import os
import shutil
import time
import datetime
import re
import glob
from timeit import default_timer as timer
from datetime import timedelta as timedelta
import subprocess
from scipy.optimize import curve_fit
from scipy.optimize import minimize

import numpy as np
import matplotlib as mlp
mlp.use('Agg')
import matplotlib.pyplot as plt
from matplotlib2tikz import save as tikz_save

# Include PyCactus3
sys.path.append('/home/kepler/bozzola/PyCactus3')
import CarpetASCIII as CARPET
import CarpetH5     as H5
import UtilityPostProcessing as PP

# Constants, in SI
G     = 6.673e-11   # m^3/(kg s^2)
c     = 299792458   # m/s
M_sun = 1.98892e30  # kg

# Conversion factors
CU_to_km   = M_sun*G/(1000*c*c)                  # km
CU_to_ms   = (1000*M_sun*G/(c*c*c))              # ms
CU_to_dens = c*c*c*c*c*c / (G*G*G * M_sun*M_sun) # kg/m^3
CU_to_dens_CGS = CU_to_dens *1000/100**3         # g/cm^3


# Function to compute phi average of function F
def phi_average(F, N, x_grid, y_grid):

    theta      = np.linspace(-np.pi/2, np.pi/2, N)

    averaged_f = np.zeros(len(x_grid))

    f = F[:,(len(F[0][:])-len(x_grid)):]

    for j,X in enumerate(x_grid):

        approximated_f = np.zeros(N)

        for i,th in enumerate(theta):

            x_target = X*np.cos(th)
            y_target = X*np.sin(th)

            i0 = np.argmax(x_grid > x_target)
            i1 = i0 - 1
            ix_box = np.array([i1, i0])
            x_box = np.array([x_grid[i1], x_grid[i0]])

            j0 = np.argmax(y_grid > y_target)
            j1 = j0 - 1

            iy_box = np.array([j1, j0])
            y_box = np.array([y_grid[j1], y_grid[j0]])

            weights = np.array([[1/((x - x_target)*(x - x_target) + (y - y_target)*(y - y_target) + 1e-20) for y in x_box] for x in y_box])
            comp_f = np.array([[f[x,y] for y in ix_box] for x in iy_box])
            normalisation = 1/np.sum(weights)
            weights *= normalisation

            approximated_f[i] = np.sum(np.multiply(weights, comp_f))

        averaged_f[j] = np.mean(approximated_f)

    return averaged_f

# Parse command line arguments

parser = argparse.ArgumentParser()

parser.add_argument("-i", "--input", type = str, required = True,
                    help = "set simulation folder")
parser.add_argument("-t", "--time", type = float, required = True,
                    help = "time of extraction in ms")
parser.add_argument("-s", "--surface", type = float,
                    help = "surface density (percent of central)")
parser.add_argument("-q", "--quadrupole", type = str,
                    help = "which quadrupole use?", choices = ["Qxx", "Qyy", "Qxy"])
parser.add_argument('--version', action='version', version='%(prog)s 1.0')

if len(sys.argv) == 1:
    parser.print_help()
    sys.exit(1)

args = parser.parse_args()

target_time     = args.time
# Default surface
if (args.surface == None):
    surface_density = 0.01
else:
    surface_density = args.surface

sim_folder        = args.input
sim_name          = sim_folder.split('/')[-1]
out_folder        = os.path.join(os.getcwd(), sim_name)
h5_folder         = os.path.join(out_folder, "h5")
q_data_folder     = os.path.join(out_folder, "q_data")
folder_2d         = sim_folder + "/o*-00??/*/data/H5_2d/*.xy.h5"

# Check if simulation folder exists
if (not os.path.isdir(sim_folder)):
    print("Simulation folder", sim_folder, "not existing!")
    sys.exit(1)

print("Working on simulation:", sim_name)

# Reading the scalars
h5_scalar_file = os.path.join(h5_folder, sim_name + "_scalar.h5")

iterations = np.loadtxt(q_data_folder + "/iterations.dat", dtype = np.int32)
time       = np.loadtxt(q_data_folder + "/time.dat")
beta       = np.loadtxt(q_data_folder + "/beta.dat")
Qxx        = np.loadtxt(q_data_folder + "/Qxx.dat")
Qyy        = np.loadtxt(q_data_folder + "/Qyy.dat")
Qxy        = np.loadtxt(q_data_folder + "/Qxy.dat")

index_maximum_beta = np.argmax(beta)
time_maximum_beta  = time[index_maximum_beta]

# Find all the maxima of the quadrupole
if (args.quadrupole == "Qxy" or args.quadrupole == None):
    q = Qxy
elif(args.quadrupole == "Qyy"):
    q = Qyy
elif(args.quadrupole == "Qxx"):
    q = Qxx

maxima_q     = np.array([])
maxima_time  = np.array([])
indexes_max  = np.array([], dtype = np.int16)

# Time distance between the a maximum and the beta maximum
dt_beta = np.array([])

for i in range(len(q)-1):
    if ((q[i] > q[i+1]) and (q[i] > q[i-1])):
        maxima_q     = np.append(maxima_q, q[i])
        maxima_time  = np.append(maxima_time, time[i])
        dt_beta      = np.append(dt_beta, np.abs(time[i] - time_maximum_beta))
        indexes_max  = np.append(indexes_max, i)

index_minimum_dt_beta = np.argmin(dt_beta)
time_zero             = maxima_time[index_minimum_dt_beta]

# Check latest time available
num_iterations = len(beta)
latest_time = time[num_iterations]

if (target_time > latest_time):
    print("The simulation has been processed only up to time {:3f} ms!".format(latest_time))
    sys.exit(1)

descXY_file = os.path.join(h5_folder, sim_name + "_descXY.h5")
descXY = H5.CreateDescriptorList(folder_2d, descXY_file)
max_rl = descXY[0]['rl'][-1]

index_iteration  = np.argmin(np.abs(time - target_time))
target_iteration = iterations[index_iteration]

dataXY_file = os.path.join(h5_folder, sim_name + "_dataXY.h5")

while True:
    try:
        dataXY = H5.CreateJoinedHDF5data(descXY, target_iteration, max_rl, dataXY_file)
    except:
        index_iteration  += 1
        target_iteration = iterations[index_iteration]
    else:
        break

print("Shot at time {:.3f} ms".format(time[index_iteration]))

dataX  = H5.Extract1dSlice(dataXY, size = 2)
dataY  = H5.Extract1dSlice(dataXY, size = 2, dir = 1)

PP.PostProcessing3dFull(dataX, kind = "X")
PP.PostProcessing3dFull(dataY, kind = "Y")
PP.PostProcessing3dFull(dataXY, kind = "XY")

zerox = np.where(dataX['X[0]'] == 0)[0][-1]

x_grid = dataX['X[0]'][zerox:]
y_grid = dataY['X[1]']

F_phi_averaged       = phi_average(dataXY['F'], 180, x_grid, y_grid)
rho_phi_averaged     = phi_average(dataXY['rho'], 180, x_grid, y_grid)
omega_phi_averaged   = phi_average(dataXY['Omega'], 180, x_grid, y_grid)
betaphi_phi_averaged = phi_average(dataXY['betaphi'], 180, x_grid, y_grid)

surface              = np.where(rho_phi_averaged >= dataX['rho'][zerox]*surface_density)[0][-1]

fig, ax = plt.subplots(1)

ax.plot(omega_phi_averaged/CU_to_ms, F_phi_averaged)
ax.text(omega_phi_averaged[-5]/CU_to_ms, F_phi_averaged[-5], "{:.3f} ms".format((time[index_iteration])))
ax.set_ylabel("F")
ax.set_xlabel("Omega [kHz]")
ax.axvline(x = omega_phi_averaged[surface]/CU_to_ms)


sys.stdout = open(os.devnull, "w")
tikz_save(sim_name + "_F_vs_Omega_phi.tikz", figureheight = '\\figureheight', figurewidth = '\\figurewidth')
sys.stdout = sys.__stdout__

plt.close(fig)

fig, ax = plt.subplots(1)

ax.set_xlabel("R [km]")
ax.set_ylabel("Omega [kHz]")
ax.plot(x_grid*CU_to_km, omega_phi_averaged/CU_to_ms)
ax.plot(x_grid*CU_to_km, -betaphi_phi_averaged/CU_to_ms)
ax.axvline(x = x_grid[surface]*CU_to_km)

sys.stdout = open(os.devnull, "w")
tikz_save(sim_name + "_Omega_R_phi.tikz", figureheight = '\\figureheight', figurewidth = '\\figurewidth')
sys.stdout = sys.__stdout__

plt.close(fig)

#!/usr/bin/env python
import os
import glob
import sys
import shutil
import numpy as np
import scipy 
import scipy.io.wavfile 
import matplotlib
import matplotlib.pyplot as plt
##import matplotlib


import CarpetASCIII as CARPET
import CarpetH5     as H5
import UtilityPostProcessing  as PP
import UtilityAnalysis        as Analysis
from   UtilityUnits import *

################################################
##
###############################################

def ProduceWaveFile(t,sig,FILENAME,timescale=1.0) :

    time = t *timescale
    timeMAX = np.max(time)
    sigMAX  = np.max(np.abs(sig))
    tAUDIO  = np.arange(0,timeMAX,1.0/44100)
    sAUDIO  = scipy.interpolate.interp1d(time,sig,kind='linear',
                              fill_value=0.0,bounds_error=False)(tAUDIO)
    scipy.io.wavfile.write(FILENAME+'.wav'
               , 44100,np.int16(sAUDIO/sigMAX*32767))

################################################################
## Invoke "ffmpeg" to produce a movie
##
## It will produce a file
## 
################################################################

def ProduceMovies(MOVIE_NAME,SEARCH_PATH,BASEDIR='.',Move=False,EraseFrame=False) :

    BASEmovieName , ext = os.path.splitext(MOVIE_NAME)
    FRAMEDIR = "MOVIES/" + BASEmovieName
    if not (os.path.exists('MOVIES')) :
        os.makedirs('MOVIES')
    if os.path.isfile("MOVIES/" + MOVIE_NAME) : 
        os.remove("MOVIES/" + MOVIE_NAME)
    if not (os.path.exists(FRAMEDIR)) :
        os.makedirs(FRAMEDIR)

    #MOVIE_NAME  = MOVIES[i]['name']
    #SEARCH_PATH = MOVIES[i]['search']
    print("SEARCH PATH= ", BASEDIR+'/'+SEARCH_PATH+'.png')
    frames=glob.glob(BASEDIR+'/'+SEARCH_PATH+'.png')
    for i,name in enumerate(np.sort(frames)):
        OUT_NAME = FRAMEDIR + '/frame_%04d.png' % (i)
        print(i,name,OUT_NAME)
        if Move == True :
           shutil.move(name,OUT_NAME)
        else :
           shutil.copy(name,OUT_NAME) 
    try :
        FIRST_FRAME=matplotlib.pyplot.imread( FRAMEDIR + '/frame_0000.png')
        y,x,C=FIRST_FRAME.shape
        SHAPE = "-s:v %dx%d" % ((x/2)*2,(y/2)*2)
        print("Shape is: ", x,y,"  ",SHAPE)
        command= ( "ffmpeg -framerate 10 -i " + FRAMEDIR + "/frame_%04d.png " 
                  + SHAPE + " -c:v libx264 -pix_fmt yuv420p " 
                  + "MOVIES/" + MOVIE_NAME )
        print("---> ",command) 
        os.system(command)
        if (EraseFrame == True ):
            for i,name in enumerate(np.sort(frames)):
                OUT_NAME = 'frames/frame_%04d.png' % (i)
                os.remove(OUT_NAME)
    except:
        print("No image for ",MOVIE_NAME," search path: ",SEARCH_PATH) 

##############################################################################
###########################  ComputeDerivedQuantity  #########################
##############################################################################

EOS_NAME = 'SLy'
def SetEOS_NAME(eosname) :
    global EOS_NAME
    EOS_NAME = eosname

def ComputeDerivedQuantity(data,var) :
    if var == 'rhoCGS' :
        data['rhoCGS'] = data['rho']*CU_to_densCGS
    elif var == 'epsrho' :
        data['epsrho'] = data['rho']*data['eps']
    elif var == 'checkEOS' :
        epsth,P = Analysis.EOS_PieceWiseEOS(data['rho'],data['eps'],eosname=EOS_NAME)
        data['checkEOS'] = np.abs(data['press'] - P )
    elif var == 'epsTHneg' :
        epsth,P = Analysis.EOS_PieceWiseEOS(data['rho'],data['eps'],eosname=EOS_NAME)
        data['epsTHneg'] = epsth
    elif var == 'epsTH' :
        epsth,P = Analysis.EOS_PieceWiseEOS(data['rho'],data['eps'],eosname=EOS_NAME)
        data['epsTH'] = (epsth*(epsth>=0))
    elif var == 'epsrhoTH' :
        if VERBOSITY > 0 : print("compute epsrhoTH")
        epsth,P = Analysis.EOS_PieceWiseEOS(data['rho'],data['eps'],eosname=EOS_NAME)
        data['epsrhoTH'] = (epsth*(epsth>=0)) * data['rho'] 


##########################################################################################
#####
##########################################################################################

BaseImageDir = ''
def SetBaseImageDir(MyBaseImageDir) :
    global BaseImageDir
    BaseImageDir = MyBaseImageDir

VERBOSITY = 0
def SetVerbosity(level) :
    global VERBOSITY
    VERBOSITY = level


TEXT_FONT_SIZE=14
HDF5_path    ='./hdf5'
HDF5_pathDESC='./hdf5_desc'
HDF5_snapshot='./snapshotH5'

def ShowBaseDir() :
    print('  dir IM.HDF5_path     is set to : ', HDF5_path)
    print('  dir IM.HDF5_pathDESC is set to : ', HDF5_pathDESC)
    print('  dir IM.HDF5_snapshot is set to : ', HDF5_snapshot)
def SetBaseDir(BASE_PATH) :
    global HDF5_path
    global HDF5_pathDESC
    global HDF5_snapshot
    HDF5_path    =os.path.join(BASE_PATH,'hdf5')
    HDF5_pathDESC=os.path.join(BASE_PATH,'hdf5_desc')
    HDF5_snapshot=os.path.join(BASE_PATH,'snapshotH5')
def SetBaseDirGalileo() :
    global HDF5_path
    global HDF5_pathDESC
    global HDF5_snapshot
    HDF5_path    ='/gpfs/work/INF14_teongrav_0/PostProcessing/hdf5'
    HDF5_pathDESC='/gpfs/work/INF14_teongrav_0/PostProcessing/hdf5_desc'
    HDF5_snapshot='/gpfs/work/INF14_teongrav_0/PostProcessing/snapshotH5'
def SetBaseKepler() :
    global HDF5_path
    global HDF5_pathDESC
    global HDF5_snapshot
    HDF5_path    ='/home/kepler/roberto.depietri/SVN/PRACE10th/PyCactus/WorkFile/hdf5'
    HDF5_pathDESC='/home/kepler/roberto.depietri/SVN/PRACE10th/PyCactus/WorkFile/hdf5_desc'
    HDF5_snapshot='/numrel/storageQ01/WorkBNS/Movies/snapshotH5'

##-----------------------------------------------------------------------------
## Default levels for SLY PP
##------------------------------------------------------------------------------
## gamma1=                   1.35692       3.005        2.988          2.851
levels1 = np.array([2.367449e-04, 8.114721e-04, 1.619100e-03])*CU_to_densCGS
## gamma2=                   1.28733       0.62223      1.35692      
levels2 = np.array([6.125960e-07, 4.254672e-06])*CU_to_densCGS
##  gamma3=                 1.58425       1.28733     
levels3 = np.array([3.95e-11])*CU_to_densCGS
##  gamma4=  ATMOSPHERE
levels4 = np.array([1.0001e-11])*CU_to_densCGS

def SetContourLevels(l1,l2,l3,l4) :
    global levels1
    global levels2
    global levels3
    global levels4
    levels1=l1   #----  k levels
    levels2=l2   #----  b levels
    levels3=l3   #----  g levels
    levels4=l4   #----  c level
    #---------------------------------------
    # For contour level of rho in CU
    #---------------------------------------


##############################################################################
################################ MAIN PLOT FUNCTION  #########################
##############################################################################
## cmap => see: http://matplotlib.org/examples/color/colormaps_reference.html
##############################################################################

def PCOLOR_IMAGE(ax,data,t_ms=(-1),var='rho',varname='',MODEL=''
    , ShowDensityContour = True , Show_km = False, extraCONTOURs = []
    , cmap='PuRd',norm = matplotlib.colors.Normalize()
    , limits = [[0.0,0.0],[0.0,0.0]]) :
    contour_list = []
    ##if not ('TEXT_FONT_SIZE' in globals()):
    ##    TEXT_FONT_SIZE = 12
    ## ----- clear plot ------
    ax.clear()
    ## -----------------------
    sc = 1.0
    CU_to_densCGS = 6.176269145886163e+17
    CU_to_km = 1.4767161818921164
    if Show_km == True :
        sc = CU_to_km
        ax.set_xlabel('(km)')
        ax.set_ylabel('(km)')
    if np.max(limits) <= 0.0:
        minX=sc*np.min(data['X[0]'])
        maxX=sc*np.max(data['X[0]'])
        minY=sc*np.min(data['X[1]'])    
        maxY=sc*np.max(data['X[1]'])
    else :
        minX=limits[0][0]
        maxX=limits[0][1]
        minY=limits[1][0]    
        maxY=limits[1][1]
    ###----------- ACTUAL PLOT --------------
    pc=ax.pcolor(sc*data['X[0]'],sc*data['X[1]'],data[var], cmap=cmap,norm=norm)
    if ShowDensityContour == True :
        ##------------------------------------------------------------------------------
        ## gamma1=                   1.35692       3.005        2.988          2.851
        levels1 = np.array([2.367449e-04, 8.114721e-04, 1.619100e-03])*CU_to_densCGS
        ## gamma2=                   1.28733       0.62223      1.35692      
        levels2 = np.array([6.125960e-07, 4.254672e-06])*CU_to_densCGS
        ##  gamma3=                 1.58425       1.28733     
        levels3 = np.array([3.95e-11])*CU_to_densCGS
        ##  gamma4=  ATMOSPHERE
        levels4 = np.array([1.0001e-11])*CU_to_densCGS
        CON1=ax.contour(sc*data['X[0]'],sc*data['X[1]'],data['rho']*CU_to_densCGS,levels=levels1,colors='k')
        CON2=ax.contour(sc*data['X[0]'],sc*data['X[1]'],data['rho']*CU_to_densCGS,levels=levels2,colors='b',linewidths=1.0)
        CON3=ax.contour(sc*data['X[0]'],sc*data['X[1]'],data['rho']*CU_to_densCGS,levels=levels3,colors='g',linewidths=1.0)
        CON4=ax.contour(sc*data['X[0]'],sc*data['X[1]'],data['rho']*CU_to_densCGS,levels=levels4,colors='c',linewidths=1.0)
        if var == 'rhoCGS' :
            if VERBOSITY > 0 : print("Adding contour")
            contour_list.append(CON1)
            contour_list.append(CON2)
            contour_list.append(CON3)
            contour_list.append(CON4)
    if len(extraCONTOURs) > 0 :
        NEWc = ax.contour(sc*data['X[0]'],sc*data['X[1]'],data[var],levels=extraCONTOURs,colors='k',linewidths=0.5)
        contour_list.append(NEWc)
    ax.set_xlim(minX,maxX)
    ax.set_ylim(minY,maxY)
    ##ax.text(0.05,0.05,'t=%5.2f' % t_ms,color='k',transform=ax.transAxes)
    ##ax.text(0.50,0.05,'var=%s' % var,color='k',transform=ax.transAxes)
    ax.text(0.05,0.05,'t=%5.2f ms' % (t_ms),color='k',transform=ax.transAxes)
    if varname != '' :
        ax.text(0.85,0.05,r'[%s]' % (varname),color='k',transform=ax.transAxes,fontsize=TEXT_FONT_SIZE,horizontalalignment='center')
        #print ((r't=%5.2f ms (%s)') % (t_ms,varname))
        #ax.text(0.05,0.05,r't=%5.2f ms (%s)' % (t_ms,varname),color='k',transform=ax.transAxes,fontsize=TEXT_FONT_SIZE)
    if MODEL != '' :
        ax.text(0.05,0.90,'%s' % (MODEL.replace('_r',' dx=0.')),color='k',transform=ax.transAxes,fontsize=TEXT_FONT_SIZE)
    return pc,contour_list

############################################################
############### END: MAIN PLOT FUNCTION  ###################
############################################################

###########################################################################################################################
##
##
##
##
##
##
###########################################################################################################################

NORM_rhoCGS_BH  = matplotlib.colors.LogNorm(vmin=0.8e7, vmax=2.63e12, clip=True) 
NORM_rhoCGS_BH2 = matplotlib.colors.LogNorm(vmin=0.8e7, vmax=1.5e10, clip=True) 
NORM_rhoCGS    = matplotlib.colors.LogNorm(vmin=0.5e10,vmax=1.5e15, clip=True)
NORM_rhoCGS2   = matplotlib.colors.LogNorm(vmin=0.5e10,vmax=5e15, clip=True)
NORM_STANDARD  = matplotlib.colors.Normalize()

def GetSnapshot(MODEL,rl,it,plane='XY',descriptor=False) :
    pass
    H5saveXY=os.path.join(HDF5_snapshot,MODEL+'_XY.h5')
    H5saveXZ=os.path.join(HDF5_snapshot,MODEL+'_XZ.h5')
    H5saveYZ=os.path.join(HDF5_snapshot,MODEL+'_YZ.h5')
    #####################################
    ###
    #####################################
    descXY = ''
    descXZ = ''
    descYZ = ''
    if descriptor == True :
        if plane == 'XY':
            descXY = H5.CreateDescriptorList('',os.path.join(HDF5_pathDESC,'descXY_' +  MODEL + '.h5'))
        elif plane == 'XZ':
            descXZ = H5.CreateDescriptorList('',os.path.join(HDF5_pathDESC,'descXZ_' +  MODEL + '.h5'))
        elif plane == 'YZ':
            descYZ = H5.CreateDescriptorList('',os.path.join(HDF5_pathDESC,'descYZ_' +  MODEL + '.h5'))
    #####################################
    ###
    #####################################
    if plane == 'XZ':
        dataXZt = H5.CreateJoinedHDF5data(descXZ,it,rl,h5_file_name=H5saveXZ)
        dataPLT = H5.ExtendDataSetMirrorZ2d(dataXZt)
    elif plane == 'YZ':
        dataYZt = H5.CreateJoinedHDF5data(descYZ,it,rl,h5_file_name=H5saveYZ)
        dataPLT = H5.ExtendDataSetMirrorZ2d(dataYZt)
    else:
        dataPLT = H5.CreateJoinedHDF5data(descXY,it,rl,h5_file_name=H5saveXY)
    return dataPLT

def ShowProduceImagesOptions():
    print("""
    OPTIONS FOR ==== > ProduceImages(dm)

    if 'lst_frames_to_show' in list(dm) : lst_frames_to_show = dm['lst_frames_to_show']
    if 'MODEL'              in list(dm) : MODEL          = dm['MODEL']
    if 'MODELname'          in list(dm) : MODELname      = dm['MODELname']
    if 'rl_toplot'          in list(dm) : rl_toplot      = dm['rl_toplot']
    if 'Plane_to_show'      in list(dm) : Plane_to_show  = dm['Plane_to_show']
    if 'img_dpi'            in list(dm) : img_dpi        = dm['img_dpi']
    if 'MYVAR'              in list(dm) : MYVAR      = dm['MYVAR']
    if 'MYVARNAME'          in list(dm) : MYVARNAME      = dm['MYVARNAME']
    if 'MYNORM'             in list(dm) : MYNORM         = dm['MYNORM']   
    if 'MYCMAP'             in list(dm) : MYCMAP         = dm['MYCMAP']   
    if 'CBARunits'          in list(dm) : CBARunits      = dm['CBARunits']       
    if 'ShowCBAR'           in list(dm) : ShowCBAR       = dm['ShowCBAR']       
    if 'ShowSCALAR'         in list(dm) : ShowSCALAR     = dm['ShowSCALAR']       
    if 'varSCALAR'          in list(dm) : varSCALAR      = dm['varSCALAR']       
    if 'tlimSCALAR'         in list(dm) : tlimSCALAR     = dm['tlimSCALAR']       
    if 'Show_km'            in list(dm) : Show_km        = dm['Show_km']       
    if 'H5saveXY'           in list(dm) : H5saveXY       = dm['H5saveXY']
    if 'H5saveXZ'           in list(dm) : H5saveXZ       = dm['H5saveXZ']
    if 'extraCONTOURs'      in list(dm) : extraCONTOURs  = dm['extraCONTOURs']

    PLOTDIR = MODEL + '_' + MYVAR +'_' + Plane_to_show +'_' +  ('rl%d' % rl_toplot)
    H5saveXY=os.path.join(HDF5_snapshot,MODEL+'_XY.h5')
    H5saveXZ=os.path.join(HDF5_snapshot,MODEL+'_XZ.h5')
    H5saveYZ=os.path.join(HDF5_snapshot,MODEL+'_YZ.h5')
    #######################################################################
    #################  Now check if PLOT dir is explicitely chosen ########
    #######################################################################
    if 'PLOTDIR'            in list(dm) : PLOTDIR        = dm['PLOTDIR']
""")

def ProduceImages(dm):
    ##dm=DATA_MOVIEs[idm]
    ##print "Elaborating model ",idm," ==>",dm
    #############################################
    ##   DEFAULT VALUES 
    #############################################
    #############################################
    MODEL          ='Sly14vs14_r25'
    MODELname      ='Sly14vs14_r25'
    rl_toplot      = 5       # a valid refinment level
    Plane_to_show  = 'XY'    # XY XZ YZ proj 
    img_dpi        = 150    
    MYVAR          = 'rhoCGS'
    MYVARNAME      = r'$\rho$'
    MYNORM         = NORM_rhoCGS
    MYCMAP         = 'PuRd'
    CBARunits      = r'$\rho$ ($g/cm^3$)'
    ShowCBAR       = True
    Show_km        = True
    extraCONTOURs  = []

    img_dpi       = 150
    rl_toplot     =5
    Plane_to_show = 'XY'
    MYVAR='rhoCGS'
    MYVARNAME=r'$\rho$'
    ## Plane_to_show = 'proj'
    ###MYNORM = matplotlib.colors.Normalize()
    MYNORM = NORM_rhoCGS
    MYCMAP = 'PuRd' 
    CBARunits=r'$\rho$ ($g/cm^3$)'
    ShowCBAR   = True
    ShowSCALAR = False
    Show_km    = True
    varSCALAR  = []
    tlimSCALAR = [] 
    lst_frames_to_show=[0]
    extraCONTOURs = []

    if 'lst_frames_to_show' in list(dm) : lst_frames_to_show = dm['lst_frames_to_show']
    if 'MODEL'              in list(dm) : 
        MODEL              = dm['MODEL']
        MODELname          = dm['MODEL']
    if 'rl_toplot'          in list(dm) : rl_toplot      = dm['rl_toplot']
    if 'Plane_to_show'      in list(dm) : Plane_to_show  = dm['Plane_to_show']
    if 'img_dpi'            in list(dm) : img_dpi        = dm['img_dpi']
    if 'MYVAR'              in list(dm) : 
        MYVAR      = dm['MYVAR']
        MYVARNAME  = MYVAR
        MYNORM     = matplotlib.colors.Normalize() # NORM_STANDARD 
        CBARunits  = '(CU)'
        #####--------   Extra setting of variable is a vel or a shift
        JETvars = {'betax' : r'$\beta_x$' , 'betay'  : r'$\beta_y$' ,'betaz' : r'$\beta_z$' ,
                   'vel[0]': r'$v_x$','vel[1]' : r'$v_y$','vel[2]' : r'$v_z$'} 
        if MYVAR in list(JETvars) :
            MYCMAP = 'seismic'
            MYVARNAME  = JETvars[MYVAR]
            extraCONTOURs = [0]
        #####--------   Extra setting of variable is the lapse
        if MYVAR == 'alp' :
            MYCMAP = 'hot'
            MYVARNAME =r"$\alpha$"           
        if MYVAR == 'H' :
            MYCMAP = 'PuRd'
            MYNORM = matplotlib.colors.Normalize() # NORM_STANDARD 
            ##extraCONTOURs = [0]
    if 'MYVARNAME'          in list(dm) : MYVARNAME      = dm['MYVARNAME']
    if 'MYNORM'             in list(dm) : MYNORM         = dm['MYNORM']   
    if 'MYCMAP'             in list(dm) : MYCMAP         = dm['MYCMAP']   
    if 'CBARunits'          in list(dm) : CBARunits      = dm['CBARunits']       
    if 'ShowCBAR'           in list(dm) : ShowCBAR       = dm['ShowCBAR']       
    if 'ShowSCALAR'         in list(dm) : ShowSCALAR     = dm['ShowSCALAR']       
    if 'varSCALAR'          in list(dm) : varSCALAR      = dm['varSCALAR']       
    if 'tlimSCALAR'         in list(dm) : tlimSCALAR     = dm['tlimSCALAR']       
    if 'Show_km'            in list(dm) : Show_km        = dm['Show_km']       
    if 'H5saveXY'           in list(dm) : H5saveXY       = dm['H5saveXY']
    if 'H5saveXZ'           in list(dm) : H5saveXZ       = dm['H5saveXZ']
    if 'extraCONTOURs'      in list(dm) : extraCONTOURs  = dm['extraCONTOURs']
    if 'MODELname'          in list(dm) : MODELname      = dm['MODELname']

    PLOTDIR = MODEL + '_' + MYVAR +'_' + Plane_to_show +'_' +  ('rl%d' % rl_toplot)
    H5saveXY=os.path.join(HDF5_snapshot,MODEL+'_XY.h5')
    H5saveXZ=os.path.join(HDF5_snapshot,MODEL+'_XZ.h5')
    H5saveYZ=os.path.join(HDF5_snapshot,MODEL+'_YZ.h5')
    #######################################################################
    #################  Now check if PLOT dir is explicitely chosen ########
    #######################################################################
    if 'PLOTDIR'            in list(dm) : PLOTDIR        = dm['PLOTDIR']
    if PLOTDIR != '' :
        if not (os.path.exists(os.path.join(BaseImageDir,PLOTDIR))) :
            os.makedirs(os.path.join(BaseImageDir,PLOTDIR))
    
    ###############################################
    ## Get data model
    ###############################################
    data   = H5.ReadScalarHDF5(os.path.join(HDF5_path,MODEL + '.h5'))
    descXY = H5.CreateDescriptorList('',os.path.join(HDF5_pathDESC,'descXY_' +  MODEL + '.h5'))
    descXZ = H5.CreateDescriptorList('',os.path.join(HDF5_pathDESC,'descXZ_' +  MODEL + '.h5'))
    descYZ = H5.CreateDescriptorList('',os.path.join(HDF5_pathDESC,'descYZ_' +  MODEL + '.h5'))
    
    its2d = descXY[0]['it'] 
    idx_its2d = list(range(len(its2d))) 
    its   = data['Sc']['iteration']
    time  = data['Sc']['time']
    it_to_time  =scipy.interpolate.interp1d(its,time)   
    tms_to_it   =scipy.interpolate.interp1d(time*CU_to_ms,its)   
    it_to_idx2d =scipy.interpolate.interp1d(its2d,idx_its2d)   
    ##----------not used -----------------------------------------------------------
    ## tms    =data['PSI4']['700']['l2m2']['time']*CU_to_ms
    ## psi4_22=data['PSI4']['700']['l2m2']['re']+1j*data['PSI4']['700']['l2m2']['im']
    ##----------not used -----------------------------------------------------------

    ################################
    ##for it in its2d[250:255:5] :
    ##for it in its2d[550:555:5] :
    ##for it in its2d[100:101] :
    ################################
    ## lst_frames_to_show is in msec
    ################################

    ############################################################################
    ### If a list of frame sto show as
    ##    0 elements ----> Produce all
    ##    1 elements and it is negative start from time -lst_frames_to_show[0]
    ##    JUST PRODUCE THE GIVEN ELEMENTS (---)
    ############################################################################
    if len(lst_frames_to_show) == 0 : 
        its = its2d
    elif (len(lst_frames_to_show) == 1 ) and (lst_frames_to_show[0] < 0 ): 
        its =  its2d[ int(it_to_idx2d(tms_to_it(-lst_frames_to_show[0]))):]
    else :
        its =  its2d[np.array(it_to_idx2d(tms_to_it(lst_frames_to_show)),dtype=np.int)]
    #################### MAIN LOOP ###################
    
    BORDER       = 0.4
    MAIN_X_SIZE  = 4.0
    sizeX        = MAIN_X_SIZE

    #############################################
    #  If we show km create an extra border space
    ##############################################
    if Show_km == True  :
        BORDER = BORDER + 0.3
    ###########################################
    #  If we whow the CBAR leave space for it 
    ###########################################
    if ShowCBAR :
        sizeX = sizeX + 1
    ###########################################
    #  If we whow SCALARs  leave space for it 
    ###########################################
    if ShowSCALAR :
        sizeX = sizeX + MAIN_X_SIZE


    ###########################################################################
    #### ------------------ SETTING SIZE of AXIS
    ###########################################################################
    
    #########################################################
    # Fix the Y size depending if it is request only a plane
    # or two projections (Scalar willl be drawn on the left)
    ########################################################## 
    if Plane_to_show=='proj' :
        ############################################
        ## Select to show XY-plane and XZ plane
        ############################################
        dataXZt = H5.CreateJoinedHDF5data(descXZ,its[0],rl_toplot,h5_file_name=H5saveXZ)
        dataPLT2= H5.ExtendDataSetMirrorZ2d(dataXZt)
        dataPLT = H5.CreateJoinedHDF5data(descXY,its[0],rl_toplot,h5_file_name=H5saveXY)
        SHAPE_IMAGE = dataPLT['X[0]'].shape
        SHAPE_IMAGE2= dataPLT2['X[0]'].shape
        if SHAPE_IMAGE[1] != SHAPE_IMAGE2[1] :
            print("Error the two image shoud have thw same X size"  ,SHAPE_IMAGE,SHAPE_IMAGE2)
        AXES_XSIZE = (MAIN_X_SIZE-BORDER-0.1)
        AXES_YSIZE = (MAIN_X_SIZE-BORDER-0.1)*SHAPE_IMAGE[0]/SHAPE_IMAGE[1]+0.1
        AXES_XSIZE2= (MAIN_X_SIZE-BORDER-0.1)
        AXES_YSIZE2= (MAIN_X_SIZE-BORDER-0.1)*SHAPE_IMAGE2[0]/SHAPE_IMAGE2[1]+0.1
        sizeY = AXES_YSIZE + AXES_YSIZE2 + 2*BORDER +0.1 
    elif Plane_to_show=='XZ' :
        dataXZt = H5.CreateJoinedHDF5data(descXZ,its[0],rl_toplot,h5_file_name=H5saveXZ)
        dataPLT = H5.ExtendDataSetMirrorZ2d(dataXZt)
        SHAPE_IMAGE = dataPLT['X[0]'].shape
        SHAPE_IMAGE2=(0.0,0.0) 
        AXES_XSIZE = (MAIN_X_SIZE-BORDER-0.1)
        AXES_YSIZE = (MAIN_X_SIZE-BORDER-0.1)*SHAPE_IMAGE[0]/SHAPE_IMAGE[1]+0.1
        sizeY = AXES_YSIZE + BORDER +0.1
    elif Plane_to_show=='YZ' :
        dataYZt = H5.CreateJoinedHDF5data(descYZ,its[0],rl_toplot,h5_file_name=H5saveYZ)
        dataPLT = H5.ExtendDataSetMirrorZ2d(dataYZt)
        SHAPE_IMAGE = dataPLT['X[0]'].shape
        SHAPE_IMAGE2=(0.0,0.0) 
        AXES_XSIZE = (MAIN_X_SIZE-BORDER-0.1)
        AXES_YSIZE = (MAIN_X_SIZE-BORDER-0.1)*SHAPE_IMAGE[0]/SHAPE_IMAGE[1]+0.1
        sizeY = AXES_YSIZE + BORDER +0.1
    else: 
        dataPLT = H5.CreateJoinedHDF5data(descXY,its[0],rl_toplot,h5_file_name=H5saveXY)
        SHAPE_IMAGE = dataPLT['X[0]'].shape
        SHAPE_IMAGE2=(0.0,0.0) 
        AXES_XSIZE = (MAIN_X_SIZE-BORDER-0.1)
        AXES_YSIZE = (MAIN_X_SIZE-BORDER-0.1)*SHAPE_IMAGE[0]/SHAPE_IMAGE[1]+0.1
        sizeY = AXES_YSIZE + BORDER +0.1

    ########## sizeY*img_dpi (150) should be divisibvle by 2
    ########## sizeY*img_dpi (150) should be divisibvle by 2
    if (sizeY*img_dpi)%2 !=0 :
        print("CHANGE SIZE [Y]")
        sizeY = ( round( (sizeY*img_dpi) / 2  ) + 1 ) * 2.0 /img_dpi
    if (sizeX*img_dpi)%2 !=0 :
        print("CHANGE SIZE [X]")
        sizeX = ( round( (sizeX*img_dpi) / 2  ) + 1 ) * 2.0 /img_dpi
    ##################################################################
    fig  =plt.figure(figsize=(sizeX,sizeY))
    if Plane_to_show=='proj' :
        axMM =fig.add_axes([BORDER/sizeX,(sizeY-AXES_YSIZE-0.1)/sizeY,AXES_XSIZE/sizeX,AXES_YSIZE/sizeY])
        axDD =fig.add_axes([BORDER/sizeX,BORDER/sizeY,AXES_XSIZE2/sizeX,AXES_YSIZE2/sizeY])
        if ShowCBAR :
            axCB =fig.add_axes([MAIN_X_SIZE/sizeX,(sizeY-AXES_YSIZE-0.1)/sizeY,0.2/sizeX,AXES_YSIZE/sizeY])
    else :
        axMM =fig.add_axes([BORDER/sizeX,BORDER/sizeY,AXES_XSIZE/sizeX,AXES_YSIZE/sizeY])
        if ShowCBAR :
            axCB =fig.add_axes([MAIN_X_SIZE/sizeX,BORDER/sizeY,0.2/sizeX,AXES_YSIZE/sizeY])

    #################################################################
    ##  If it is sequest to show scalar create the coorespong axis
    #################################################################
    axSC  = []
    axVAR = []
    axOPT = []
    axTIME= data['Sc']['time']*CU_to_ms
    if len(tlimSCALAR) != 2 :
        tlimSCALAR = [np.min(axTIME),np.max(axTIME)]
    if ShowSCALAR :
        NvarSC = len(varSCALAR)
        X_SIZE_SCALAR = (MAIN_X_SIZE - BORDER)
        Y_SIZE_SCALAR = (sizeY-BORDER-0.2)/NvarSC
        for idx in range(NvarSC):
            if type(varSCALAR[idx][1]) != list : 
                valVAR=scipy.interpolate.interp1d(varSCALAR[idx][0],varSCALAR[idx][1],fill_value=0.0,bounds_error=False)(axTIME)
            else :
                valVAR =[scipy.interpolate.interp1d(varSCALAR[idx][0],values,fill_value=0.0,bounds_error=False)(axTIME)
                         for values in varSCALAR[idx][1]]
                pass;
            axVAR.append(valVAR)
            newAX=fig.add_axes([(sizeX-MAIN_X_SIZE+BORDER-0.2)/sizeX,
                                (BORDER+(idx)*Y_SIZE_SCALAR)/sizeY,
                                 X_SIZE_SCALAR/sizeX,
                                 Y_SIZE_SCALAR/sizeY])
            axSC.append(newAX)
            if len(varSCALAR[idx]) > 2 :
                axOPT.append(varSCALAR[idx][2])
            else :
                axOPT.append({})

        pass;



    ###########################################################################
    ## Now that the axis has been created generate the plots
    ###########################################################################
    for it in its :
        tms =it_to_time(it)*CU_to_ms
        ###################################
        ##  Read data for 2d section
        ###################################
        if Plane_to_show=='proj' :
            dataXZt = H5.CreateJoinedHDF5data(descXZ,it,rl_toplot,h5_file_name=H5saveXZ)
            dataPLT2= H5.ExtendDataSetMirrorZ2d(dataXZt)
            dataPLT = H5.CreateJoinedHDF5data(descXY,it,rl_toplot,h5_file_name=H5saveXY)
        elif Plane_to_show=='XZ' :
            dataXZt = H5.CreateJoinedHDF5data(descXZ,it,rl_toplot,h5_file_name=H5saveXZ)
            dataPLT = H5.ExtendDataSetMirrorZ2d(dataXZt)
        elif Plane_to_show=='YZ' :
            dataYZt = H5.CreateJoinedHDF5data(descYZ,it,rl_toplot,h5_file_name=H5saveYZ)
            dataPLT = H5.ExtendDataSetMirrorZ2d(dataYZt)
        else:
            dataPLT = H5.CreateJoinedHDF5data(descXY,it,rl_toplot,h5_file_name=H5saveXY)
        ###----------------------------------------------------
        ## If variable has a special name, ====>
        ##    Compute it using special rules 
        ###----------------------------------------------------
        if not (MYVAR in list(dataPLT) ) :
            pass
            ComputeDerivedQuantity(dataPLT,MYVAR)  
            if Plane_to_show=='proj' :
                ComputeDerivedQuantity(dataPLT2,MYVAR)  
        if MYVAR == 'H' :
            dataPLT['H'] *= np.where(np.abs(dataPLT['H']) > 1e100 , 0.0,1.0)
            if Plane_to_show=='proj' :
                dataPLT2['H'] *= np.where(np.abs(dataPLT2['H']) > 1e100 , 0.0,1.0)
        ###----------------------------------------------------
        ###
        ###----------------------------------------------------
        pc, contour_list = PCOLOR_IMAGE(axMM,dataPLT,tms,var=MYVAR,varname=MYVARNAME,MODEL=MODELname,
                          norm=MYNORM,cmap=MYCMAP,extraCONTOURs = extraCONTOURs,Show_km=Show_km)
        if Plane_to_show=='proj' :
            pc2,xxx =  PCOLOR_IMAGE(axDD,dataPLT2,tms,var=MYVAR,varname=MYVARNAME,MODEL=MODELname,
                          norm=MYNORM,cmap=MYCMAP,extraCONTOURs = extraCONTOURs,Show_km=Show_km)
        ###----------------------------------------------------
        ###
        ###----------------------------------------------------
        if ShowCBAR :
            CCB=fig.colorbar(pc,ax=axMM,cax=axCB,format='%3.1E')  
            CCB.ax.set_xlabel(CBARunits,fontsize=12,horizontalalignment='left')
            CCB.ax.yaxis.set_tick_params(width=3,length=5)
            CCB.ax.tick_params(labelsize=10)
            for NEWCONTOUR in contour_list :
                if VERBOSITY > 0 : print("Adding extra contours")
                try :
                    CCB.add_lines(NEWCONTOUR,erase=False)
                except:
                    pass; 
        ###----------------------------------------------------
        ###
        ###----------------------------------------------------
        if ShowSCALAR :
            pass;
            IDX0 = np.where(axTIME <= tms)[0]
            for idx,AXES in enumerate(axSC):
                AXES.clear()
                if type(axVAR[idx]) != list : 
                    AXES.plot(axTIME,axVAR[idx],'k',lw=0.25)
                    tmpLINE,=AXES.plot(axTIME[IDX0],axVAR[idx][IDX0],'b')
                else :
                    for values in axVAR[idx]:
                        AXES.plot(axTIME,values,'k',lw=0.25)
                        tmpLINE,=AXES.plot(axTIME[IDX0],values[IDX0],'b')
                    pass;
                for opt in axOPT[idx]:
                    if opt == 'ylim' :
                        AXES.set_ylim(axOPT[idx][opt])
                        #AXES.plot([tms,tms],axOPT[idx][opt],'y',lw=4)
                    ######## Option for line
                    elif opt == 'color' :
                        tmpLINE.set_color(axOPT[idx][opt])
                    elif opt == 'ls' :
                        tmpLINE.set_ls(axOPT[idx][opt])
                    elif opt == 'lw' :
                        tmpLINE.set_lw(axOPT[idx][opt])
                    elif opt == 'dashes' :
                        tmpLINE.set_dashes(axOPT[idx][opt])
                    ######## Option for axes
                    elif opt == 'ylabel' :
                        AXES.set_ylabel(axOPT[idx][opt])
                    elif opt == 'yticks' :
                        AXES.set_yticks(axOPT[idx][opt])
                    elif opt == 'yticklabels' :
                        AXES.set_yticklabels(axOPT[idx][opt])
                    elif opt == 'text' :
                        AXES.text(0.1,0.1,axOPT[idx][opt],transform=AXES.transAxes,fontsize=8)
                 
                AXES.set_xlim(tlimSCALAR)
                if idx > 0 :
                    AXES.set_xticklabels([]) 
                else :
                    AXES.set_xlabel('t (ms)')
                AXES.plot([tms,tms],AXES.get_ylim(),'y',lw=4)
                AXES.grid()
        ###----------------------------------------------------
        ###  Draw the plot and save the image file
        ###----------------------------------------------------
        plt.draw()
        if PLOTDIR=='' :
            pass;
            PLOT_FILE_name = 'None'
        else:
            PLOT_FILE_name = ( 'img_'+ MYVAR +'_' + Plane_to_show +'_' + 
                              ('rl%d' % rl_toplot) +'_' + ('%06d' % it) + '.png'
                             )
            FULL_PLOT_FILE_name = os.path.join(BaseImageDir,PLOTDIR,PLOT_FILE_name)
            plt.savefig(FULL_PLOT_FILE_name,dpi=img_dpi)
            if VERBOSITY > 0 : print("Exporting file: ",PLOT_FILE_name," in dir ",PLOTDIR)
    return ( PLOTDIR )


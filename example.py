#!/usr/bin/env python

##--------------------------------
import sys
import glob
import re
import os
import numpy as  np
import matplotlib.pyplot as plt
from matplotlib import rc
##--------------------------------


sys.path.append('../PyCactus')


import CarpetASCIII as CARPET
import CarpetH5     as H5

import UtilityPostProcessing as PP

# dataSC = H5.ReadScalarHDF5('../Sly_14vs14_r25/Sly14vs14_r25.h5')
# datah5XY = dict()
# datah5XZ = dict()

# PATH='/gpfs/work/INF17_teongrav/gbozzola/'
# MODEL='Sly_14vs14_r25'
# SEARCH3d='/output-0000/*/data/H5_3d/*.xyz.file_0.h5'

# SEARCH2dXY='/output-0000/*/data/H5_2d/*.xy.h5'
# fullSEARCH2dXY = PATH + MODEL + SEARCH2dXY
# descXY_FILE='hdf5/''descXY' + MODEL + '.h5'
# dataXY_FILE='hdf5/''dataXY' + MODEL + '.h5'

# SEARCH2dXZ='/output-0000/*/data/H5_2d/*.xz.h5'
# fullSEARCH2dXZ = PATH + MODEL + SEARCH2dXZ
# descXZ_FILE='hdf5/''descXZ' + MODEL + '.h5'
# dataXZ_FILE='hdf5/''dataXZ' + MODEL + '.h5'

# it=0;rl=5;

# descXY = H5.CreateDescriptorList(fullSEARCH2dXY,descXY_FILE)
# datah5XY[it,rl] = H5.CreateJoinedHDF5data(descXY,it,rl,dataXY_FILE)

# descXZ = H5.CreateDescriptorList(fullSEARCH2dXZ,descXZ_FILE)
# datah5XZ[it,rl] = H5.CreateJoinedHDF5data(descXZ,it,rl,dataXZ_FILE)


h2dir = '/gpfs/work/INF17_teongrav/gbozzola/Sly_14vs14_r25/output-0003/Sly_14vs14_r25/data/H5_2d/'
h3dir = '/gpfs/work/INF17_teongrav/gbozzola/Sly_14vs14_r25/output-0003/Sly_14vs14_r25/data/H5_3d/'

SEARCH2d='*.??.h5'
SEARCH3d ='*.???.file_0*.h5'
SEARCH3d0='*.???.h5'

# rhoXY =H5.CreateDescriptorList(h2dir+'rho.xy.h5')
# rhoXYZ=H5.CreateDescriptorList(h3dir+'rho.xyz.file_0.h5')

allXY =H5.CreateDescriptorList(h2dir+'*.xy.h5', h2dir + 'descXY.h5')
# allXYZ=H5.CreateDescriptorList(h3dir+'*.xyz.file_0.h5')

# it0_3d = H5.CreateJoinedHDF5data(allXYZ,0,5)
it0_2d = H5.CreateJoinedHDF5data(allXY,234496,5,  h2dir + 'dataXY.h5')
it0_2dx = H5.Extract2dSlice(it0_3d,0,dir=2)
PP.PostProcessing3dFull(it0_2d, "XY")
fig, (ax1, ax2, ax3) = plt.subplots(1,3)
print("R")
print("len(Rcyl)",len(it0_2d['Rcyl']))
print("len(Rcyl[0])",len(it0_2d['Rcyl'][0]))
print(it0_2d['Omega'][133])
print("X")
# print "len(X[0])",len(it0_2d['X[0]'])
# print "len(X[0][0])",len(it0_2d['X[0]'][0])
# print it0_2d
# for key in it0_2d :
#     print key
print(np.shape(it0_2d['F']))
ax3.plot(it0_2d['Omega'][0], it0_2d['F'][0])
ax2.plot(it0_2d['X[0]'][0], it0_2d['Omega'][0])
ax1.pcolor(it0_2d['X[0]'],it0_2d['X[1]'],it0_2d['rho'])
fig.savefig("test.png")
plt.show()

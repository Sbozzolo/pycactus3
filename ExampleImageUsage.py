#!/usr/bin/env python 
#%load_ext autoreload
#%autoreload 2

import os
import sys
import re


##########################################################
##  Try possible PyCactus path and set it (if it exist)
#########################################################
MACHINE_NAME=''
PostProcessingDIR= {
 'TYCHO'  :'/Users/depietri/PostProcessing',
 'KEPLER' :'/numrel/storageQ02/Work_BNS16/PostProcessing',
 'MIKE'   :'/work/depietri/PostProcessing',
 'GALILEO':'/gpfs/work/INF14_teongrav_0/Parma/PostProcessing'
}
PyCactus_PATHs = [
   [ '/Users/depietri/SVN/BNS2016/PyCactus/'              , 'TYCHO'  ],
   [ '/home/kepler/roberto.depietri/SVN/BNS2016/PyCactus/', 'KEPLER' ],
   [ '/home/depietri/PyCactus/'                           , 'MIKE'   ],
   [ '/gpfs/work/INF14_teongrav_0/Parma/PyCactus/'        , 'GALILEO']
]
### end of possible paths ###############################################
for PyCactus_PATH, machine in PyCactus_PATHs:
    print("Trying path : ",PyCactus_PATH)
    if os.path.isdir(PyCactus_PATH) :
        sys.path.append(PyCactus_PATH)
        MACHINE_NAME = machine
        print("Current Machine is : ",   MACHINE_NAME)
        print("Setting the PyCactus path on sys.path.append:", PyCactus_PATH)  
        break
##########################################################

if MACHINE_NAME == 'MIKE' :
    import matplotlib as mpl
    mpl.use('Agg')

############################################
###  PYCACTUS load
############################################

from   UtilityUnits import *
import CarpetASCIII as CARPET
import CarpetH5     as H5
import UtilityPostProcessing   as PP
import UtilityAnalysis         as Analysis
import UtilityImages           as IM
import CarpetSimulationData    as CC
############################################
##  Not needed
## import UtilityPostProcessing as PP
## import UtilityEOS            as EOS
############################################

if MACHINE_NAME!= '' :
    PPdir = PostProcessingDIR[MACHINE_NAME]
    print("Setting the base dir for HDF5 file to", PPdir)
    IM.SetBaseDir(PPdir)
    print('  dir IM.HDF5_path     is set to : ',IM.HDF5_path)
    print('  dir IM.HDF5_pathDMACHINE_NAMEESC is set to : ',IM.HDF5_pathDESC)
    print('  dir IM.HDF5_snapshot is set to : ',IM.HDF5_snapshot)

########################################################################
#IM.SetBaseDir('/Users/depietri/TEST')
########################################################################

if MACHINE_NAME == 'KEPLER' :
    name  ='Sly14vs14_r50'
    simdir='/numrel/storageQ02/WorkBNS/simulations/DePietriGalileo/Sly_14vs14_r50'
elif MACHINE_NAME == 'TYCHO' :
    name  ='Sly14vs14_r25'
    simdir='/Users/depietri/simulations/BSSN_G300_14vs14_r75'
elif MACHINE_NAME == 'GALILEO' :
    #-----------------------------------------------------------------------
    # mkdir /gpfs/work/INF14_teongrav_0/Parma/PostProcessing/hdf5
    # mkdir /gpfs/work/INF14_teongrav_0/Parma/PostProcessing/hdf5_desc 
    # mkdir /gpfs/work/INF14_teongrav_0/Parma/PostProcessing/snapshotH5
    #-----------------------------------------------------------------------
    ## qsub -I  -A INF14_teongrav_0 -l walltime=1:00:00 -l select=1:ncpus=1:mem=32gb
    ## module load gnu blas lapack python numpy matplotlib scipy autoload profile/advanced h5py ipython ffmpeg
    name = 'Sly_14vs14_r38'
    simdir = '/gpfs/work/INF14_teongrav_0/rdepietr/simulations/Sly_14vs14_r38'
elif MACHINE_NAME == 'MIKE' :
    #-----------------------------------------------------------------------
    #[depietri@mike2 Cactus]$ mkdir  /work/depietri/PostProcessing
    #[depietri@mike2 Cactus]$ mkdir  /work/depietri/PostProcessing/hdf5
    #[depietri@mike2 Cactus]$ mkdir  /work/depietri/PostProcessing/hdf5_desc
    #[depietri@mike2 Cactus]$ mkdir  /work/depietri/PostProcessing/snapshotH5
    #-----------------------------------------------------------------------
    ## qsub -I -A hpc_hyrel15  -l nodes=1:ppn=16  -q checkpt -l walltime=1:00:00 
    name  ='ALF2pp_126vs173_d44_r375'
    simdir='/work/depietri/simulations/ALF2pp_126vs173_d44_r375'
    pass
#################### SPECIAL CASE FOR QUEEN BEE ============
if os.getenv('HOSTNAME')[:2] == 'qb':
    ##---qsub -I -A loni_hyrel15  -l nodes=1:ppn=20  -q checkpt -l walltime=1:00:00
    ## module load python
    name = 'MS1pp_125vs170_d44_r3125'
    simdir='/work/depietri/simulations/MS1pp_125vs170_d44_r3125/'
    pass
#################### SPECIAL CASE FOR QUEEN BEE ============
 

IM.SetBaseImageDir('./Images')
sc   = CC.ReadAllScalarsSimulation( name,simdir,IM.HDF5_path)
desXY= CC.ReadDescriptorsSimulation(name,simdir,basedirH5=IM.HDF5_pathDESC,kind='XY',reload=False) 
desXZ= CC.ReadDescriptorsSimulation(name,simdir,basedirH5=IM.HDF5_pathDESC,kind='XZ',reload=False) 
desYZ= CC.ReadDescriptorsSimulation(name,simdir,basedirH5=IM.HDF5_pathDESC,kind='YZ',reload=False) 
OUT_DIR=IM.ProduceImages( {'MODEL':name,'lst_frames_to_show' : [0] })
###IM.ProduceMovies('pippo.mp4','*',OUT_DIR)

import numpy as np
import h5py
import sys
sys.path.append("./romspline")
import romspline
from UtilityUnits import *
import UtilityAnalysis as Analysis
from CarpetSimulationData import *
#import lal


def LigoWaveFromModel(model,folderH5='./',outdir="/numrel/storageQ03/2016/LIGO_format_waveforms/"):
    d=model
    d.update(ReadAllScalarsSimulation(d['long_name'],d['basedir'],folderH5,reload=False))
    f0=d['Omega0']/np.pi
    psi_inf2 = Analysis.ExtrapolateAllPsi4(d['PSI4'],d['M0'],order_extr=2,int_method='filter',f0=f0)
    try:
        r0="700"
        d['time'] = Analysis.RetardedTime(d['PSI4'][r0]['l2m2']['time'],int(r0),M=d['M0'])
        d['dt']=np.diff(pp['time'][r])[1]
        d['h']=Analysis.NIntegrateAllPsi4(psi_inf2[r0],2,M=d['M0'],int_method='filter',f0=f0)
    except:
        r0="400"
        d['time'] = Analysis.RetardedTime(d['PSI4'][r0]['l2m2']['time'],int(r0),M=d['M0'])
        d['dt']=np.diff(d['time'])[1]
        d['h']=Analysis.NIntegrateAllPsi4(psi_inf2[r0],2,M=d['M0'],int_method='filter',f0=f0)

    merg_time=Analysis.FindMaximumTimeSeries(d['time'],d['h']['l2m2'])
    d['tm']=merg_time[0]
    idx0 = np.searchsorted(d['time']*CU_to_ms,0)
    idx_junk = np.searchsorted(d['time']*CU_to_ms,1)
    idx_end = np.searchsorted(d['time'],d['time'][-1]-1/CU_to_ms)
    time_junkless = d['time'][idx_junk:idx_end]-d['tm']
    H5file = h5py.File(outdir+d['name']+".h5","a")
    for mode in d['PSI4'][r0]:
        h_junkless=d['h'][mode][idx_junk:idx_end]
        A = np.abs(h_junkless)
        phi = np.unwrap(np.angle(h_junkless))
        phi = phi-phi[0]
        spline_A = romspline.ReducedOrderSpline(time_junkless,A)
        spline_phi = romspline.ReducedOrderSpline(time_junkless,phi)
        mode_name=mode[:2]+'_'+mode[2:]
        A_group = H5file.create_group("amp_"+mode_name)
        phi_group = H5file.create_group("phase_"+mode_name)
        spline_A.write(A_group,slim=True)
        spline_phi.write(phi_group,slim=True)
    #t_group = H5file.create_group("NR_time")
    #t_group.create_dataset("t[Msun]",data=time_junkless)
    H5file.attrs.create("NR-group","GRHydro_BSSN_WENO_Parma")
    H5file.attrs.create("type","non spinning")
    H5file.attrs.create("name",d['name'])
    H5file.attrs.create("object1","NS")
    H5file.attrs.create("object2","NS")
    try:
        H5file.attrs.create("mass1",d["M"])
        H5file.attrs.create("mass2",d["M"])
    except:
        H5file.attrs.create("mass1",d["M2"])
        H5file.attrs.create("mass2",d["M1"])
    try:
        H5file.attrs.create("eta",d["M1"]*d["M2"]/(d["M1"]+d["M2"])**2)
    except:
        H5file.attrs.create("eta",0.25)
    H5file.attrs.create("spin1x",0)
    H5file.attrs.create("spin1y",0)
    H5file.attrs.create("spin1z",0)
    H5file.attrs.create("spin2x",0)
    H5file.attrs.create("spin2y",0)
    H5file.attrs.create("spin2z",0)
    H5file.attrs.create("LNhatx",0)
    H5file.attrs.create("LNhaty",0)
    H5file.attrs.create("LNhatz",1)
    H5file.attrs.create("nhatx",1)
    H5file.attrs.create("nhaty",0)
    H5file.attrs.create("nhatz",0)
    H5file.attrs.create("f_lower_at_1MSUN",f0/CU_to_ms*1000)
    H5file.attrs.create("coa_phase",0)


def LigoWaveFromH5File(h5_filename,name,omega0,m1,m2):
    print("Not ready yet")
    #TODO: takes H5 filename and attributes as inputs, creates MODELS temporary file, calls LigoWaveFromModel

def LigoWaveFromSim(folder):
    print("Not ready yet")
    #TODO: takes simfactory root folder as input, creates MODELS temporary file, calls LigoWaveFromModel

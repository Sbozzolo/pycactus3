#!/usr/bin/env python

##--------------------------------
import sys
import glob
import re
import os
import argparse
import numpy as  np
import matplotlib.pyplot as plt
from matplotlib import rc
import scipy.fftpack
import CarpetASCIII as CARPET
import CarpetH5     as H5
import UtilityPostProcessing as PP
##--------------------------------

#constants, in SI
G     = 6.673e-11   # m^3/(kg s^2)
c     = 299792458   # m/s
M_sun = 1.98892e30  # kg
# Conversion factors
CU_to_km   = M_sun*G/(1000*c*c)                  # km
CU_to_ms   = (1000*M_sun*G/(c*c*c))              # ms
CU_to_dens = c*c*c*c*c*c / (G*G*G * M_sun*M_sun) # kg/m^3
CU_to_dens_CGS = CU_to_dens *1000/100**3         # g/cm^3

# Use CU
# CU_to_km   = 1
# CU_to_ms   = 1
# CU_to_dens = 1
# CU_to_dens_CGS = 1


# if plt.rcParams["text.usetex"] is False:
#     plt.rcParams["text.usetex"] = True

# if plt.rcParams["text.latex.unicode"] is False:
#     plt.rcParams["text.latex.unicode"] = True

# if "siunitx" not in plt.rcParams["text.latex.preamble"]:
#     plt.rcParams["text.latex.preamble"].append(r"\usepackage{siunitx}")


parser = argparse.ArgumentParser()
parser.add_argument("-p", "--path", type = str, help = "full path of the folder of the simulation",
                    required = True)
parser.add_argument("-m", "--model", type = str, help = "model name (to create a specific dir)")
parser.add_argument("-o", "--output", type = int, help = "in which of the output file is the iteration requested?",
                    required = True)
parser.add_argument("-i", "--iteration", help = "iteration when to extract the rotation law",
                    required = True,  type = int)
# parser.add_argument("-s", "--surface", help = "density (CU) of the surface",
#                     required = True, type = float)
parser.add_argument('--version', action='version', version='%(prog)s 1.0')

if (len(sys.argv) == 1):
    parser.print_help()
    sys.exit(1)

args = parser.parse_args()
path = args.path
ite  = args.iteration
# surf = args.surface
out  = str(args.output)

if (not os.path.exists(path)):
    print("Path", path, "not readable")
    sys.exit(1)

# if (not ite % 1024 == 0):
#     print "Number of iteration has to be multiple of 1024"
#     sys.exit(1)

sys.path.append('../PyCactus')

h2dir = os.path.join(path)
h2dir = h2dir + "/output-00" + out.zfill(2) + "/*/data/H5_2d/"

print(h2dir)

if (not args.model == None):
    model_name = args.model
else:
    model_name = path.split('/')[-1]

folder = os.path.join(os.getcwd(), model_name, out.zfill(2))

if not os.path.exists(folder):
    print("MAKE DIR", folder)
    os.makedirs(folder)

# Raw data

print("H2DIR", h2dir + '*.xy.h5')

allXY = H5.CreateDescriptorList(h2dir + '*.xy.h5', os.path.join(folder, 'descXY.h5'))
dataXY = H5.CreateJoinedHDF5data(allXY, ite, 5, os.path.join(folder, 'dataXY.h5'))

print(dataXY)

# Data on the y = 0, z = 0 axis
dx = H5.Extract1dSlice(dataXY, size = 2)
dy = H5.Extract1dSlice(dataXY, size = 2, dir = 1)

# Post process
PP.PostProcessing3dFull(dx, kind = "X")
PP.PostProcessing3dFull(dy, kind = "Y")

# Index of the center
zerox = np.where(dx['X[0]'] == 0)[0][-1]
zeroy = np.where(dy['X[1]'] == 0)[0][-1]

# Surface is the index of the last element with rho > than 0.01 rho_central
surfacex = np.where(dx['rho'] >= dx['rho'][zerox]*0.01)[0][-1]
surfacey = np.where(dy['rho'] >= dy['rho'][zeroy]*0.01)[0][-1]

# Global figure with the situation and two plots
fig = plt.figure()
ax1 = plt.subplot2grid((2,2), (0,0))
ax2 = plt.subplot2grid((2,2), (1,1))
ax3 = plt.subplot2grid((2,2), (1,0))
ax4 = plt.subplot2grid((2,2), (0,1))

# Densities
ax1.axis('equal')
ax1.contour(dataXY['X[0]']*CU_to_km,dataXY['X[1]']*CU_to_km,dataXY['rho']*CU_to_dens_CGS)
ax1.pcolor(dataXY['X[0]']*CU_to_km,dataXY['X[1]']*CU_to_km,dataXY['rho']*CU_to_dens_CGS)
ax1.set_xlabel("x [km]")
ax1.set_ylabel("y [km]")
ax1.text(-10, 35, str(round(dx['time']/204,3)) + " ms", color = 'white')

ax2.set_xlabel("R [km]")
ax2.set_ylabel("Omega [kHz]")
ax2.set_xlim([0, np.amax(dx['X[0]'])*CU_to_km])
ax2.set_ylim([0,12])
ax2.axvline(x = dx['X[0]'][surfacex]*CU_to_km, color = 'black')
ax2.axvline(x = dy['X[1]'][surfacey]*CU_to_km, color = 'red')
ax2.plot(dx['X[0]'][zerox:]*CU_to_km, dx['Omega'][zerox:]/CU_to_ms)
ax2.plot(dy['X[1]'][zeroy:]*CU_to_km, dy['Omega'][zeroy:]/CU_to_ms, color = 'red')

ax3.set_xlabel("Omega [kHz]")
ax3.set_ylabel("F(Omega) [ms]")
ax3.set_ylim([0,1.4])
# ax3.set_xlim([np.amin(d['Omega'][zero:])/CU_to_ms, np.amax(d['Omega'][zero:])/CU_to_ms])
ax3.set_xlim([0,13])
ax3.plot(dx['Omega'][zerox:]/CU_to_ms, dx['F'][zerox:]*CU_to_ms)
ax3.plot(dy['Omega'][zeroy:]/CU_to_ms, dy['F'][zeroy:]*CU_to_ms, color = 'red')
ax3.axvline(x = dx['Omega'][surfacex]/CU_to_ms, color = 'black')
ax3.axvline(x = dy['Omega'][surfacey]/CU_to_ms, color = 'black')
# ax3.axvspan(d['Omega'][surface]/CU_to_ms, d['Omega'][len(d['Omega']) - 1]/CU_to_ms, alpha = 0.1, color = 'blue')

ax4.set_xlabel("R [km]")
ax4.set_ylabel("rho [g/cm^3]")
ax4.set_xlim([0, np.amax(dx['X[0]'])*CU_to_km])
ax4.set_ylim([0, np.amax(dx['rho'][zerox:])*CU_to_dens_CGS])
ax4.axvline(x = dx['X[0]'][surfacex]*CU_to_km)
ax4.axvline(x = dy['X[1]'][surfacey]*CU_to_km, color = 'red')
ax4.plot(dx['X[0]'][zerox:]*CU_to_km, dx['rho'][zerox:]*CU_to_dens_CGS)
ax4.plot(dy['X[1]'][zeroy:]*CU_to_km, dy['rho'][zeroy:]*CU_to_dens_CGS, color = 'red')

pics = os.path.join(folder, "pics")

if not os.path.exists(pics):
    os.makedirs(pics)

fig.tight_layout()
if (not os.path.exists(os.path.join(pics, str(ite).zfill(7) + ".png"))):
    fig.savefig(os.path.join(pics, str(ite).zfill(7) + ".png"))

dat = os.path.join(folder, "data")

if not os.path.exists(dat):
    os.makedirs(dat)

# Save iteration Omega and F(Omega)
if (not os.path.exists((os.path.join(dat, str(ite).zfill(7) + "_" + str(dx['time']) + "_" + str(surfacex)+ "_Omega_x.txt.gz")))):
    np.savetxt(os.path.join(dat, str(ite).zfill(7) + "_" + str(dx['time']) + "_" + str(surfacex) + "_Omega_x.txt.gz"), dx['Omega'])
if (not os.path.exists((os.path.join(dat, str(ite).zfill(7) + "_" + str(dx['time']) + "_" + str(surfacex) +  "_F_x.txt.gz")))):
    np.savetxt(os.path.join(dat, str(ite).zfill(7) + "_" + str(dx['time']) + "_" + str(surfacex) + "_F_x.txt.gz"), dx['F'])
if (not os.path.exists((os.path.join(dat, str(ite).zfill(7) + "_" + str(dy['time']) + "_" + str(surfacey) + "_Omega_y.txt.gz")))):
    np.savetxt(os.path.join(dat, str(ite).zfill(7) + "_" + str(dy['time']) + "_" + str(surfacey) +  "_Omega_y.txt.gz"), dy['Omega'])
if (not os.path.exists((os.path.join(dat, str(ite).zfill(7) + "_" + str(dy['time']) + "_" + str(surfacey) + "_F_y.txt.gz")))):
    np.savetxt(os.path.join(dat, str(ite).zfill(7) + "_" + str(dy['time']) + "_" + str(surfacey) + "_F_y.txt.gz"), dy['F'])

#!/usr/bin/env python

import sys
import getopt

import os
import re
import glob
import time
import numpy as np
import bz2
import gzip
import h5py as h5py
from matplotlib import pyplot as plt

PackakeDescription = """
   Here the description of the main function defined in this module:

   descr=CreateDescriptorList("/path_to_files/*.xyz.file_0.h5',"file.h5") :
         Save in the h5 file names all the information on all
         the component present that match the path
   dsets=CreateJoinedHDF5data(descr,it,rl,h5_file_name='',mirror_z=False)
         Given a descriptor list create the joined datasets (one for each
         element of the list) correspondig to
         * it = Iteration Number
         * rl = Refinment level
         * mirror_z == True then extended data (full-grid)
         The generated data are saved in the hdf5 file for faster RELOAD
   idx=FindIndex(dsets,x,dir=0)
         Return the index of the closet element in the direction "i"

   SearchComponents(DIR,name)

"""

#------------------------------------------------------
#
# General notes: the indexing follow the rule that
#
# val(i2,i1,i0) correspond to the point:
#               i[0] = x = x( :  ,  : , i0 )
#               i[1] = y = y( :  , i1 , :  )
#               i[2] = z = z( i2 ,  : , :  )
#------------------------------------------------------

#################################################################################
###
### Store values used by the routine
###
### symmetry.mirror_z : contains a dict() of known simmetry properties for z->-z
###
#################################################################################

class symmetries :
    pass
symmetries.mirror_z= {
  'vel[0]' : +1.0 ,
  'vel[1]' : +1.0 ,
  'vel[2]' : -1.0 ,
  'betax'  : +1.0 ,
  'betay'  : +1.0 ,
  'betaz'  : -1.0 ,
  'Bvec[0]' : -1.0 ,
  'Bvec[1]' : -1.0 ,
  'Bvec[2]' : +1.0 ,
  'Bx' : -1.0 ,
  'By' : -1.0 ,
  'Bz' : +1.0
}
symmetries.pysim= {
  'vel[0]' : -1.0 ,
  'vel[1]' : -1.0 ,
  'vel[2]' : +1.0 ,
  'betax'  : -1.0 ,
  'betay'  : -1.0 ,
  'betaz'  : +1.0 ,
  'Bvec[0]' : -1.0 ,
  'Bvec[1]' : -1.0 ,
  'Bvec[2]' : +1.0 ,
  'Bx' : -1.0 ,
  'By' : -1.0 ,
  'Bz' : +1.0
}

class search :
    pass
search.OUTPUT   ='(\S*)/output-(\d\d\d\d)/(\S*)'
search.COMPONENT='(\S*).file_(\d*).(\S*)'
search.Name_COMP='(\S*).file_(\d*).'
search.it_tl_rl   = '(\S*)::(\S*) it=(\d*) tl=(\d*) rl=(\d*)\S*'
search.it_tl_rl_c = '(\S*)::(\S*) it=(\d*) tl=(\d*) rl=(\d*) c=(\d*)\S*'

#search.it_tl_rl   = '(\S*)::(\S*) it=('+str(it)+') tl=(\d*) rl=('+str(rl)+')\S*'
#search.it_tl_rl_c = '(\S*)::(\S*) it=('+str(it)+') tl=(\d*) rl=('+str(rl)+')\S*'

#################################################################################
###
###  Definition for:
###
###     SearchComponents(DIR,name='rho')
###     SearchDirOutput(File) ==> File or list of restart [dir0,....,dirN]
###             If File is a "FILE" return File
###             If file is a "DIR"  return a list od dir
##                     whose name match: search.OUTPUT ='(\S*)/output-(\d\d\d\d)/(\S*)'
###     CreateDescriptor(ROOT_file)
###     CreateDescriptorList(h5_file_name,pattern) :
##              From a pattern return a list of descriptor for all the variable
##              matching pattern (Use "CreateDescriptor(...)")
#################################################################################

def SearchComponents(DIR,name='rho') :
     if os.path.isfile(DIR) :
         path, File = os.path.split(DIR)
         x = re.search(search.COMPONENT,File)
         if x :
             ##print x.groups()[0]
             files  = os.listdir(path)
             REfilter  = re.compile(search.Name_COMP + x.groups()[2] )
             REcheck = lambda xx : xx.find(x.groups()[0]) >=0
             component = list(filter(REcheck,list(filter(REfilter.match, files))))
             ##print component
         else:
             component = [File]
     else :
         path   = DIR
         files  = os.listdir(path)
         REfilter  = re.compile(search.COMPONENT)
         REcheck = lambda xx : xx.find(name) >=0
         component = list(filter(REcheck,list(filter(REfilter.match, files))))
         if len(component) == 0 :
             component = list(filter(REcheck, files))
     return path,component

### -------------------------------------------------------
###
### -------------------------------------------------------

def SearchDirOutput(File) :
     if os.path.isfile(File) :
         path, name = os.path.split(File)
     elif os.path.isdir(File) :
         path = File
     else :
         return  []
     x = re.search(search.OUTPUT,path)
     if x :
         restarts = glob.glob(x.groups()[0]+'/output-????/'+x.groups()[2])
     else :
         restarts = []
     # Eliminate empty folders from the list
     restarts = [path for path in restarts if (len(os.listdir(path)) > 0)]
     return restarts

### -------------------------------------------------------
###
### -------------------------------------------------------

def CreateDescriptor(ROOT_file) :
    path,comps=SearchComponents(ROOT_file)
    paths=SearchDirOutput(ROOT_file)
    nfiles = len(paths)*len(comps)
    print(len(paths),len(comps),nfiles,ROOT_file)
    class desc:
         pass;
    files     = dict()
    comp_addr = dict()
    it_set    = set()
    tl_set    = set()
    rl_set    = set()
    c_set     = set()
    #
    data  = dict()
    data['files'] = np.ndarray(nfiles,dtype="|S256")
    #
    i = -1
    for path in paths :
        for comp in comps :
             pass;
             i += 1
             INdata   = GetTimes(os.path.join(path,comp))
             #print data
             #print path,comp
             #print os.path.join(path,comp)
             it_set = it_set.union( set(list( INdata['it'] )) )
             tl_set = tl_set.union( set(list( INdata['tl'] )) )
             rl_set = rl_set.union( set(list( INdata['rl'] )) )
             c_set  = c_set.union(  set(list( INdata['c']  )) )
             ## ---------------
             files[i] = INdata['file']
             data['files'][i] = INdata['file']
             for it in INdata['it'] :
                 for rl in INdata['rl'] :
                     for c in INdata['c'] :
                         comp_addr[it,rl,c] = i
    data['group'] = ("{}".format((str(INdata['group'])))).encode("UTF-8")
    data['name']  = ("{}".format((str(INdata['var'])))).encode("UTF-8")
    data['it']    = np.unique(list(it_set))
    data['rl']    = np.unique(list(rl_set))
    data['tl']    = np.unique(list(tl_set))
    data['c']     = np.unique(list(c_set))
    data['addr']  = np.ndarray( (len(data['it']),len(data['rl']),len(data['c'])),dtype=np.int32 )
    idx_it=dict()
    idx_rl=dict()
    idx_c =dict()
    for i,it in enumerate(data['it']): idx_it[it]=i
    for i,rl in enumerate(data['rl']): idx_rl[rl]=i
    for i,c  in enumerate(data['c']):  idx_c[c] =i
    for y in comp_addr:
        data['addr'][idx_it[y[0]],idx_rl[y[1]],idx_c[y[2]] ] = comp_addr[y]
    return data

def CreateDescriptorList(pattern,h5_file_name='') :
    if  ( (h5_file_name != '' )  &
          (os.path.isfile(h5_file_name) == True )    ) :
        f = h5py.File(h5_file_name)
        x = list(f)
        f.close()
        OUT = [ReadHDF5(h5_file_name,group=xx) for xx in x]
    else :
        XLIST=glob.glob(pattern)
        # print "XLIST of the files:", XLIST
        XLIST2 = [file for file in XLIST if "output-0000" in file]
        print(XLIST2)
        OUT =  [ CreateDescriptor(file) for file in XLIST2]
        print("LEN(OUT)", len(OUT))
        if (h5_file_name != '' ) :
            for i in range(len(OUT)):
                # DIRTY NASTY HACK
                # It is knonw that there is a problem with Unicode, so it is better
                # to encode the strings and not have bytestrings
                # https://github.com/h5py/h5py/issues/289
                # Strings like ADMBASE have to be converted
                group = "{}".format(OUT[i]['name'])
                group = group[2:-1]
                print("GROUP", group)
                SaveHDF5(h5_file_name,OUT[i],group = group)
    return OUT

#################################################################################
###
###  FindExtendCoordinateRL(desc,rl,it=0):
###        Given a VAR hdf5 descriptor return the extend of the coordinate associate
###        to the given refinement level of the VAR
###  ReadDataSets(descs,it,rl,mirror_z=False)                      ==> dsets
###  CreateJoinedHDF5data(descr,it,rl,h5_file_name='',mirror_z=False) ==> dsets
###
###  --> GenerateCoordinatesVal(coords)
###      FindComponentIndex(coords,x,axes='x')
###      FindIndex(dsets,x,dir=0)
###      ExtendDataSetMirrorZ(dsets)  ==> dsets
#################################################################################


######### -----------------------------------------------
#########
######### -----------------------------------------------

def FindExtendCoordinateRL(desc,rl,it=0):
    idx_it=dict()
    idx_rl=dict()
    idx_c =dict()
    for i,Xit in enumerate(desc['it']):  idx_it[Xit]=i
    for i,Xrl in enumerate(desc['rl']):  idx_rl[Xrl]=i
    for i,Xc  in enumerate(desc['c']):   idx_c[Xc] =i
    csets=dict()
    IDX0 = 0
    for comp in desc['c']:
        try :
            #print "Reading it",idx_it[it]
            file_idx   = desc['addr'][idx_it[it],idx_rl[rl],idx_c[comp]]
            file_name  = (desc['files'][file_idx]).decode('UTF-8')
            # VERY VERY NASTY AND DIRTY HACK!
            group_name = "{}".format(desc['group'])
            group_name = group_name[2:-1]
            var_name   = "{}".format(desc['name'])
            var_name = var_name[2:-1]

            #print desc.comp_addr[it,rl,comp]
            #print desc.files[desc.comp_addr[it,rl,comp]]

            csets[IDX0] = ReadHDF5(file_name,group=DSETname(group_name,var_name,it,0,rl,comp),coords=True,values=False)
            IDX0 = IDX0 +1
        except Exception as e:
            # print(str(e))
            pass;
    IDX0 = 0
    origin = np.array(csets[IDX0]['coords']['origin'])
    shape  = np.array(csets[IDX0]['coords']['shape' ])
    delta  = np.array(csets[IDX0]['coords']['delta' ])
    corner = np.array(csets[IDX0]['coords']['corner'])
    for IDX0 in list(csets):
        origin =np.min([csets[IDX0]['coords']['origin'],origin],axis=0)
        corner =np.max([csets[IDX0]['coords']['corner'],corner],axis=0)
    for i in range(len(origin)):
        shape[i]=np.int(np.round((corner[i]-origin[i])/delta[i]+1))
    coords = {'origin':origin,'shape':shape,'delta':delta,'corner':corner}
    #### GenerateCoordinatesVal(coords)
    return coords

######### -----------------------------------------------
#########
######### -----------------------------------------------

def ReadDataSets(descs,it,rl,mirror_z=False) :
    if type(descs) != list :
       descs=[descs]
    desc  = descs[0]
    coord = FindExtendCoordinateRL(desc,rl,it)
    ## ---------------------------------------
    if (mirror_z == True ) :
       coord['origin'][2] = - coord['corner'][2]
       coord['shape'][2]  = np.int(np.round(2*coord['corner'][2]/coord['delta'][2])) + 1

    xs,Xs = GenerateCoordinatesVal(coord)
    val = np.zeros( Xs[0].shape)

    OUT = dict()
    if len(Xs)== 3 :
       OUT['X[0]'] = Xs[0]
       OUT['X[1]'] = Xs[1]
       OUT['X[2]'] = Xs[2]
       OUT['x[0]'] = xs[0]
       OUT['x[1]'] = xs[1]
       OUT['x[2]'] = xs[2]
    else :
       OUT['X[0]'] = Xs[0]
       OUT['X[1]'] = Xs[1]
       OUT['x[0]'] = xs[0]
       OUT['x[1]'] = xs[1]

    ## print "val shape (global) ",val.shape
    ## ---------------------------------------------------------
    ##  KEEP IN MIND that this data follows FORTRAN CONVENTION
    ##
    ##    ['val'].shape       = (dim_z,dim_y,dim_x)
    ##    ['coords']['shape'] = (dim_x,dim_y,dim_z)
    ##
    ## ---------------------------------------------------------

    ## -------------------------------------
    ##     Now fill in the val matrix
    ## -------------------------------------
    for desc in descs :
        csets=dict()
        for comp in desc['c']:
            try:
                #print "Reading it",idx_it[it]
                idx_it=dict()
                idx_rl =dict()
                idx_c =dict()
                for i,Xit in enumerate(desc['it']):  idx_it[Xit]=i
                for i,Xrl in enumerate(desc['rl']):  idx_rl[Xrl]=i
                for i,Xc  in enumerate(desc['c']):   idx_c[Xc] =i
                file_idx   = desc['addr'][idx_it[it],idx_rl[rl],idx_c[comp]]
                file_name  = (desc['files'][file_idx]).decode('UTF-8')
                # VERY VERY NASTY AND DIRTY HACK!
                group_name = "{}".format(desc['group'])
                group_name = group_name[2:-1]
                var_name   = "{}".format(desc['name'])
                var_name = var_name[2:-1]
                #print desc.comp_addr[it,rl,comp]
                #print desc.files[desc.comp_addr[it,rl,comp]]
                dset = ReadHDF5(file_name,group=DSETname(group_name,var_name,it,0,rl,comp),coords=True,values=True)
                time = dset['attrs']['time']
                ## print "Time is: ",time
                Global_IDX = np.round((dset['coords']['origin']-coord['origin'])/coord['delta'])
                # Make sure that in Global_IDX all the numbers are integers
                Global_IDX = np.array([np.int(x) for x in Global_IDX])
                # print Global_IDX
                # print val.shape
                dimension=len(Global_IDX)
                if (dimension == 3 ) :
                    IDX_0 = np.int(dset['coords']['shape'][0])
                    IDX_1 = np.int(dset['coords']['shape'][1])
                    IDX_2 = np.int(dset['coords']['shape'][2])
                    val[Global_IDX[2]:(Global_IDX[2]+IDX_2),Global_IDX[1]:(Global_IDX[1]+IDX_1),Global_IDX[0]:(Global_IDX[0]+IDX_0)] = dset['val'][:,:,:]
                elif (dimension == 2 ) :
                    IDX_0 = np.int(dset['coords']['shape'][0])
                    IDX_1 = np.int(dset['coords']['shape'][1])
                    val[Global_IDX[1]:(Global_IDX[1]+IDX_1),Global_IDX[0]:(Global_IDX[0]+IDX_0)] = dset['val'][:,:]
            except Exception as e:
                # print "Exception for: ",group_name,var_name,it,0,rl,comp
                # # Which exception?
                # print (str(e))
                pass
        ## ---------------------------------------
        if (mirror_z == True ) :
            try :
                PARITY = symmetries.mirror_z[var_name]
            except:
                PARITY = 1.0
            idx_z_0     = FindComponentIndex(coord,0,axes='z')
            val[0:idx_z_0,:,:] = PARITY * val[-1:idx_z_0:-1,:,:]
            ### -----Please Check !!
        OUT[var_name] = np.array(val)
        OUT['time']   = np.array(time)
    return OUT

######### -----------------------------------------------
#########
######### -----------------------------------------------


def CreateJoinedHDF5data(descr,it,rl,h5_file_name='',mirror_z=False,saveSinglePrecision=False) :
    group_name = "it="+str(it)+" rl="+str(rl)
    # print("GROUP NAME:", group_name)
    present = False
    if  (h5_file_name == '' ):
        present = False
    elif os.path.isfile(h5_file_name) == True :
        f = h5py.File(h5_file_name)
        x = list(f)
        try:
            x=type(f[group_name])
            present = True
        except:
            present = False
        f.close()
    ##############
    if (present == True ):
        OUT=ReadHDF5(h5_file_name,group=group_name)
    else:
        OUT=ReadDataSets(descr,it,rl,mirror_z)
        if  (h5_file_name != '' ):
            SaveHDF5(h5_file_name,OUT,group=group_name,saveSinglePrecision=saveSinglePrecision)
    return OUT

def READJoinedHDF5data(h5_file_name,it,rl) :
    group_name = "it="+str(it)+" rl="+str(rl)
    present = False
    if os.path.isfile(h5_file_name) == True :
        f = h5py.File(h5_file_name,'r')
        x = list(f)
        try:
            x=type(f[group_name])
            present = True
        except:
            present = False
        f.close()
    ##############
    if (present == True ):
        print("Reading ",group_name , " from file ",h5_file_name)
        OUT=ReadHDF5(h5_file_name,group=group_name)
    else:
        OUT= dict()
        print(group_name , "Not found")
    return OUT

######### -----------------------------------------------
#########
######### -----------------------------------------------

def ExtendDataSetMirrorZ(data) :
    Nz,Ny,Nx = data['X[0]'].shape
    minX,maxX=np.min(data['X[0]']),np.max(data['X[0]'])
    minY,maxY=np.min(data['X[1]']),np.max(data['X[1]'])
    minZ,maxZ=np.min(data['X[2]']),np.max(data['X[2]'])
    print(Nx,Ny,Nz)
    coords = dict()
    coords['origin'] = np.array([minX,minY,minZ])
    coords['shape' ] = np.array([Nx,Ny,Nz])
    coords['delta' ] = np.array([(maxX-minX)/(Nx-1),(maxY-minY)/(Ny-1),(maxZ-minZ)/(Nz-1)])
    coords['corner'] = np.array([maxX,maxY,maxZ])
    idx_z_0 = FindComponentIndex(coords,0,axes='z')
    coords['origin'][2] = - coords['corner'][2]
    coords['shape'][2]  = np.int(np.round(2*coords['corner'][2]/coords['delta'][2])) + 1
    idx_z_1 = FindComponentIndex(coords,0,axes='z')
    print(idx_z_0,idx_z_1)
    xs,Xs = GenerateCoordinatesVal(coords)
    val = np.zeros( Xs[0].shape)
    OUT = dict()
    if len(Xs)== 3 :
       OUT['X[0]'] = Xs[0]
       OUT['X[1]'] = Xs[1]
       OUT['X[2]'] = Xs[2]
       OUT['x[0]'] = xs[0]
       OUT['x[1]'] = xs[1]
       OUT['x[2]'] = xs[2]
    else :
       OUT['X[0]'] = Xs[0]
       OUT['X[1]'] = Xs[1]
       OUT['x[0]'] = xs[0]
       OUT['x[1]'] = xs[1]
    for i in list(data) :
        if ( ( i == 'X[0]' )  | ( i == 'X[1]' ) | ( i == 'X[2]' )
           | ( i == 'x[0]' )  | ( i == 'x[1]' ) | ( i == 'x[2]' )
           ) :
            pass
        elif (i == 'time' ) :
            OUT[i] = np.array(data[i])
        else :
            pass;
            OUT[i] = np.zeros(Xs[0].shape)
            OUT[i][idx_z_1:,:,:] = data[i][idx_z_0:,:,:]
            try :
                PARITY = symmetries.mirror_z[i]
                #print "Parity ",PARITY, "applied to var ",i
            except:
                PARITY = 1.0
            OUT[i][0:idx_z_1,:,:] = PARITY * OUT[i][-1:idx_z_1:-1,:,:]
    return OUT

######### -----------------------------------------------
#########
######### -----------------------------------------------

def ExtendDataSetMirrorZ2d(data) :
    Nz,Nx = data['X[0]'].shape
    minX,maxX=np.min(data['X[0]']),np.max(data['X[0]'])
    minZ,maxZ=np.min(data['X[1]']),np.max(data['X[1]'])
    print(Nx,Nz)
    coords = dict()
    coords['origin'] = np.array([minX,minZ])
    coords['shape' ] = np.array([Nx,Nz])
    coords['delta' ] = np.array([(maxX-minX)/(Nx-1),(maxZ-minZ)/(Nz-1)])
    coords['corner'] = np.array([maxX,maxZ])
    idx_z_0 = FindComponentIndex(coords,0,axes='y')
    coords['origin'][1] = - coords['corner'][1]
    coords['shape'][1]  = np.int(np.round(2*coords['corner'][1]/coords['delta'][1])) + 1
    idx_z_1 = FindComponentIndex(coords,0,axes='y')
    # print(idx_z_0,idx_z_1)
    xs,Xs = GenerateCoordinatesVal(coords)
    val = np.zeros( Xs[0].shape)
    OUT = dict()
    OUT['X[0]'] = Xs[0]
    OUT['X[1]'] = Xs[1]
    OUT['x[0]'] = xs[0]
    OUT['x[1]'] = xs[1]
    for i in list(data) :
        if ( ( i == 'X[0]' )  | ( i == 'X[1]' )
           | ( i == 'x[0]' )  | ( i == 'x[1]' )
           ) :
            pass
        elif (i == 'time' ) :
            OUT[i] = np.array(data[i])
        else :
            pass;
            OUT[i] = np.zeros(Xs[0].shape)
            OUT[i][idx_z_1:,:] = data[i][idx_z_0:,:]
            try :
                PARITY = symmetries.mirror_z[i]
                # print("Parity ",PARITY, "applied to var ",i)
            except:
                PARITY = 1.0
            OUT[i][0:idx_z_1,:,] = PARITY * OUT[i][-1:idx_z_1:-1,:]
    return OUT

######### -----------------------------------------------
#########
######### -----------------------------------------------

def ExtendData2dPYSIM(data) :
    Nz,Nx = data['X[0]'].shape
    minX,maxX=np.min(data['X[0]']),np.max(data['X[0]'])
    minZ,maxZ=np.min(data['X[1]']),np.max(data['X[1]'])
    # print(Nx,Nz)
    coords = dict()
    coords['origin'] = np.array([minX,minZ])
    coords['shape' ] = np.array([Nx,Nz])
    coords['delta' ] = np.array([(maxX-minX)/(Nx-1),(maxZ-minZ)/(Nz-1)])
    coords['corner'] = np.array([maxX,maxZ])
    idx_x_0 = FindComponentIndex(coords,0,axes='x')
    coords['origin'][0] = - coords['corner'][0]
    coords['shape'][0]  = np.int(np.round(2*coords['corner'][0]/coords['delta'][0])) + 1
    idx_x_1 = FindComponentIndex(coords,0,axes='x')
    # print(idx_x_0,idx_x_1)
    xs,Xs = GenerateCoordinatesVal(coords)
    val = np.zeros( Xs[0].shape)
    OUT = dict()
    OUT['X[0]'] = Xs[0]
    OUT['X[1]'] = Xs[1]
    OUT['x[0]'] = xs[0]
    OUT['x[1]'] = xs[1]
    for i in list(data) :
        if ( ( i == 'X[0]' )  | ( i == 'X[1]' )
           | ( i == 'x[0]' )  | ( i == 'x[1]' )
           ) :
            pass
        elif (i == 'time' ) :
            OUT[i] = np.array(data[i])
        else :
            pass;
            OUT[i] = np.zeros(Xs[0].shape)
            OUT[i][:,idx_x_1:] = data[i][:,idx_x_0:]
            try :
                PARITY = symmetries.pysim[i]
                # print("Parity ",PARITY, "applied to var ",i)
            except:
                PARITY = 1.0
            OUT[i][::-1,0:idx_x_1,] = PARITY * OUT[i][:,-1:idx_x_1:-1]
    return OUT

######### -----------------------------------------------
#########
######### -----------------------------------------------

def Extract2dSlice(dsets,x = 0,dir=0) :
    """
    Extract a two-dimensional slice from a 3D set on the plane defined by the value
    of x in the direction dir
    """
    idx=FindIndex(dsets,x,dir)
    OUT = dict()
    for i in list(dsets) :
        if ( ( i == 'x[0]' )  | ( i == 'x[1]' ) | ( i == 'x[2]' )
           ) :
            pass
        elif (i == 'time' ) :
            OUT[i] = np.array(dsets[i])
        else :
            if (dir == 0):
                OUT[i] = np.array(dsets[i][:,:,idx])
            elif (dir == 1):
                OUT[i] = np.array(dsets[i][:,idx,:])
            elif (dir == 2):
                OUT[i] = np.array(dsets[i][idx,:,:])
    return OUT

def Extract1dSlice(dsets, x = [0,0,0], dir = 0, size = 3) :
    """Extract a one-dimensional slice from a 2D or 3D set (defined by size) in the
    direction dir passing through the point x

    """
    idx0=FindIndex(dsets,x[0],0)
    idx1=FindIndex(dsets,x[1],1)
    if (size == 3):
        idx2=FindIndex(dsets,x[2],2)
    OUT = dict()
    for i in list(dsets) :
        if ( ( i == 'x[0]' )  | ( i == 'x[1]' ) | ( i == 'x[2]' )
           ) :
            pass
        elif (i == 'time') :
            OUT[i] = np.array(dsets[i])
        else :
            if (dir == 0):
                if (size == 3):
                    OUT[i] = np.array(dsets[i][idx2,idx1,:])
                elif (size == 2):
                    OUT[i] = np.array(dsets[i][idx1,:])
            elif (dir == 1):
                if (size == 3):
                    OUT[i] = np.array(dsets[i][idx2,:,idx0])
                elif (size == 2):
                    OUT[i] = np.array(dsets[i][:,idx0])
            elif (dir == 2):
                OUT[i] = np.array(dsets[i][:,idx1,idx0])
    return OUT

## -------------------------------------------------------------
##
##  Given ['coords'] descriptor it generate coordinate array
##
##
##  KEEP IN MIND that this data follows FORTRAN CONVENTION
##
##    ['val'].shape       = (dim_z,dim_y,dim_x)
##    ['coords']['shape'] = (dim_x,dim_y,dim_z)
##
## -------------------------------------------------------------

def GenerateCoordinatesVal(coords) :
    #------------------------------------------------------
    #
    # General notes: the indexing follow the rule that
    #
    # val(i2,i1,i0) correspond to the point:
    #               i[0] = x = x( :  ,  : , i0 )
    #               i[1] = y = y( :  , i1 , :  )
    #               i[2] = z = z( i2 ,  : , :  )
    #------------------------------------------------------
    origin = coords['origin']
    shape  = coords['shape' ]
    delta  = coords['delta' ]
    corner = coords['corner']
    coord = dict()
    grid  = dict()
    #
    # ---- compute the range of the coordinates
    #
    for i in range(len(origin)) :
        coord[i] = np.linspace(origin[i],corner[i],shape[i])
    ## -----------------------------------------
    ## We do need to check if this work when the
    ## size in X and Y are different
    ## (if the grid is 3-dimensional)
    ## -----------------------------------------
    #if len(origin) == 3:
    #    grid[2],grid[1],grid[0] = np.meshgrid( coord[2],coord[1],coord[0],indexing='ij')
    #else :
    #    grid[1],grid[0] = np.meshgrid(coord[1],coord[0],indexing='ij')
    ######################################################################################
    ##  Not using the indexing options require:
    ######################################################################################
    if len(origin) == 3:
        grid[1],grid[2],grid[0] = np.meshgrid( coord[1],coord[2],coord[0])
    else :
        grid[0],grid[1] = np.meshgrid(coord[0],coord[1])



    return coord,grid

######### -----------------------------------------------
#########
######### -----------------------------------------------

def FindComponentIndex(coords,x,axes='x') :
    origin = coords['origin']
    shape  = coords['shape' ]
    delta  = coords['delta' ]
    corner = coords['corner']
    if axes=='x' :
        dir = 0
    elif axes=='y' :
        dir = 1
    elif axes=='z' :
        dir = 2
    else :
        return -1
    if (x < origin[dir] ) | ( x > corner[dir] ) :
        return (-1)
    else :
        idxs = 1.0*np.array(list(range(shape[dir])))
        vals = origin[dir] + idxs * delta[dir]
        return int(round(np.interp(x,vals,idxs)))

######### -----------------------------------------------
#########
######### -----------------------------------------------

def FindIndex(dsets,x,dir=0) :
    vals = dsets['x['+str(dir)+']']
    idxs = np.array(list(range(len(vals))))
    return int(round(np.interp(x,vals,idxs)))


#################################################################################
###
###
###  SaveHDF5(file,data,group='',mode='a')
###  ReadHDF5(file,group='',coords=False,values=True)
###
###
#################################################################################

### -------------------------------------------------------
###
### -------------------------------------------------------

def SaveHDF5(file,data,group='',mode='a',saveSinglePrecision=False) :
    f = h5py.File(file, mode)
    # print("GROUP", group)
    # print("KEYS", list(f.keys()))
    if group in list(f.keys()):
        f.close()
        print("No, IO ESCO!")
        return
    if len(group) > 0 :
        try:
            grp = f[group]
        except:
            grp = f.create_group(group)
    else :
       grp = f

    for varname in sorted(list(data)) :
       try :
           if ( saveSinglePrecision == False ) :
               grp.create_dataset(varname, data = data[varname])
           else :
               if (type(data[varname]) == np.ndarray) :
                   XDATA=np.array(data[varname],dtype=np.dtype('float32'))
                   grp.create_dataset(varname, data=XDATA)
               else :
                   grp.create_dataset(varname, data = data[varname])
       except Exception as e:
           pass;
    f.close()

### -----------------------------------------------------------------------------
##  This rooutine read a group of data on an HDF5 file and return a dictionary
##  of the values of the data present on the group
##
##  IF (group) IS A DATASETS return a dictiory with the
##
##  -->  ["val"]   the values of the datasets
##       ["attrs"] its attributes (as a dictionary)
##
##  and if (coords=True) THE CARPET CORDINATES ATTRIBUTES deduced from "attrs"
##       ['coords']['shape']  = shape
##       ['coords']['origin'] = origin
##       ['coords']['corner'] = corner
##       ['coords']['delta']  = delta
##
##  KEEP IN MIND that this data follows FORTRAN CONVENTION
##
##    ['val'].shape       = (dim_z,dim_y,dim_x)
##    ['coords']['shape'] = (dim_x,dim_y,dim_z)
##
### -----------------------------------------------------------------------------

def ReadHDF5(file,group='',coords=False,values=True) :
    f = h5py.File(file,'r')
    if len (group) > 0 :
       grp = f[group]
    else :
       grp = f
    data = dict()
    # ------------------------------------------------------------
    #  If the specified group is a CARPET dataset output the data
    #  and the attributes associated to the data
    #
    #  Otherwise read all datasets in the group
    # ------------------------------------------------------------
    if type(grp) == h5py._hl.dataset.Dataset :
        if (values == True):
            data['val']   = np.array(grp)
        ### ---------------------------
        ### Reading attributes
        ### ---------------------------
        data['attrs'] =dict()
        for att in list(grp.attrs) :
            data['attrs'][att] = np.array(grp.attrs[att])
        #
        # ---- If requested compute the coordinate array
        #
        if coords :
            #------------------------------------------------------
            #
            # General notes: the indexing follow the rule that
            #
            # val(i2,i1,i0) correspond to the point:
            #               i[0] = x = x( :  ,  : , i0 )
            #               i[1] = y = y( :  , i1 , :  )
            #               i[2] = z = z( i2 ,  : , :  )
            #------------------------------------------------------
            data['coords'] =dict()
            #shape  = np.array(data['val'].shape)[-1::-1]
            shape  = np.array(grp.shape)[-1::-1]
            origin = data['attrs']['origin']
            ##### ----- print "shape: ", shape, "origin: ", origin
            delta  = data['attrs']['delta']
            corner = origin+delta*(shape-1)
            data['coords']['shape']  = shape
            data['coords']['origin'] = origin
            data['coords']['corner'] = corner
            data['coords']['delta']  = delta
            #
            # ---- compute the range of the coordinates
            #
            #for i in range(len(origin)) :
            #    data['coords'][ 'range',i] = linspace(origin[i],corner[i],shape[i])
            #if len(data['coords']['shape']) == 3:
            #    data['coords'][ 'coord',2],data['coords'][ 'coord',1],data['coords'][ 'coord',0] = np.meshgrid(
            #              data['coords'][ 'range',2],data['coords'][ 'range',1],data['coords'][ 'range',0],indexing='ij')
            #else :
            #   data['coords'][ 'coord',1],data['coords'][ 'coord',0] = np.meshgrid(
            #              data['coords'][ 'range',1],data['coords'][ 'range',0],indexing='ij')
        # ------------------------------------------------------
        #
        # ------------------- END READING CARPET dataset
        #
        # ------------------------------------------------------
    else :
        for varname in sorted(list(grp)) :
            if type(grp[varname]) == h5py._hl.dataset.Dataset :
                data[varname] = np.array(grp[varname])
    f.close()
    return data

#################################################################################
###
###  GetTimes(file) :                Used by  CreateDescriptor(ROOT_file)
###  DSETname(GROUP,var,it,tl,rl,c)  Used by  FindExtendCoordinateRL(desc,rl):
###  Properties_DSETname(dset)
###
###
#################################################################################

### -------------------------------------------------------
###
### -------------------------------------------------------


def DSETname(GROUP,var,it,tl,rl,c) :
    if c >= 0 :
        return GROUP+'::'+var+' it='+str(it)+' tl='+str(tl)+' rl='+str(rl)+' c='+str(c)
    else :
        return GROUP+'::'+var+' it='+str(it)+' tl='+str(tl)+' rl='+str(rl)

### -------------------------------------------------------
###
### -------------------------------------------------------

def Properties_DSETname(dset) :
     x= re.search(search.it_tl_rl,dset)
     if x != None :
         x1 = re.search(search.it_tl_rl_c,dset)
         if x1 != None :
             GROUP,var,it,tl,rl,c =  x1.groups()
         else :
              GROUP,var,it,tl,rl = x.groups()
              c = -1
         return GROUP,var,int(it),int(tl),int(rl),int(c)
     # --------- ERROR ----
     return -1

### -------------------------------------------------------
###
### -------------------------------------------------------

def GetTimes(file) :
     it_set =set()
     tl_set =set()
     rl_set =set()
     c_set  =set()
     print("Get times from file:",file)
     h5=h5py.File(file,'r')
     for dset in list(h5):
         x= re.search(search.it_tl_rl,dset)
         if x != None :
             x1 = re.search(search.it_tl_rl_c,dset)
             if x1 != None :
                 GROUP,var,it,tl,rl,c =  x1.groups()
             else :
                  GROUP,var,it,tl,rl = x.groups()
                  c = -1
             # time = np.real(h5[dset].attrs['time'])
             # print dset,'AT it=',it,'tl=',tl,'rl=',rl,'c=',c,'   time=',time
             it_set.add(np.int(it))
             tl_set.add(np.int(tl))
             rl_set.add(np.int(rl))
             c_set.add(np.int(c))
         #else :
             #print dset
     its = np.sort(np.array(list(it_set)))
     rls = np.sort(np.array(list(rl_set)))
     tls = np.sort(np.array(list(tl_set)))
     cs  = np.sort(np.array(list(c_set)))
     #for i in range(len(its)) :
     #     dset= DSETname(GROUP,var,its[i],tls[0],rls[0],cs[0])
     #     print dset
     #     print '..... time=', h5[dset].attrs['time']
     #     times= np.array( [np.real(h5[DSETname(GROUP,var,its[i],tls[0],rls[0],cs[0])].attrs['time']) for i in range(len(its)) ])
     h5.close()
     #    return {'it':its,'tl':tls,'rl': rls,'c': cs,'time':times,'group':GROUP,'var':var}
     return {'it':its,'tl':tls,'rl': rls,'c': cs,'group':GROUP,'var':var,'file':file}



#################################################################################
###
###
###  def ReadScalarHDF5(file,group='') :
###  def WriteScalaHDF5(file,data,group='',mode='a') :
###
###
#################################################################################

def WriteDictionaryVar(h5f,data) :
    ##print type(data)
    for varname in sorted(list(data)) :
        x_data = data[varname]
        x_type = type(x_data)
        ##print x_type
        if x_type == dict :
            ## print "START Recursing var=",varname
            grp = h5f.create_group(varname)
            WriteDictionaryVar(grp,x_data)
            ## print "END Recursing var=",varname
        else:
            if x_type != np.ndarray :
                ## print "Converting to NDARRAY var ", varname
                values = np.array(x_data)
            else :
                values = x_data
            ##print "(saving var ",varname,")",type(values),"-",values.dtype
            try :
                h5f.create_dataset(varname, data=values)
            except:
                print("Error creating dataset ",varname)

def ReadDictionaryVar(h5f) :
    ## print type(data)
    ## <class 'h5py._hl.files.File'>
    ## <class 'h5py._hl.group.Group'>
    ## <class 'h5py._hl.dataset.Dataset'>
    out = dict()
    for i in sorted(list(h5f)) :
        if    type(h5f[i]) == h5py._hl.dataset.Dataset :
            out[i] = np.array(h5f[i])
        elif  type(h5f[i]) == h5py._hl.group.Group :
            grp = h5f[i]
            out[i] = ReadDictionaryVar(grp)
            pass;
    return out

def ReadScalarHDF5(file,group='') :
    try:
      f = h5py.File(file,'r')
    except:
      print(("Cannot read from file %s" % file))
      sys.exit(1)
    if len (group) > 0 :
       grp = f[group]
    else :
       grp = f
    # ------------------------------------------------------------
    #  If the specified group is a CARPET dataset output the data
    #  and the attributes associated to the data
    #
    #  Otherwise read all datasets in the group
    # ------------------------------------------------------------
    if type(grp) == h5py._hl.dataset.Dataset :
        out = np.array(grp)
    else :
        out = ReadDictionaryVar(grp)
    f.close()
    return out

def WriteScalarHDF5(file,data,group='',mode='a') :
    f = h5py.File(file,mode)
    if len (group) > 0 :
       try:
           grp = f.create_group(group)
       except :
           grp = f[group]
       grp = f[group]
    else :
       grp = f
    # ------------------------------------------------------------
    #
    # ------------------------------------------------------------
    WriteDictionaryVar(grp,data)
    f.close()
    return 1


#################################################################################
###
###
###    M A I N
###
###
#################################################################################

if __name__ == "__main__":
    pass;
